#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
#
#
# Copyright (C) 2011, 2012 Domingos Rodrigues <ddcr@lcc.ufmg.br>
#
# Created: Mon Nov 21 16:08:26 2011 (BRST)
#
# $Id$
#
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
# import plotsettings

__id__ = "$Id:$"
__revision__ = "$Revision:$"


def create_envelope(x1, x2, x3, x4, x5, y1, y2, y3, y4, y5):
    """
    all this just to find the envelope region that encloses the set of points
    """
    all_x = np.hstack((x1, x2, x3, x4, x5))
    all_y = np.hstack((y1, y2, y3, y4, y5))
    _cache = {}
    for i, v in zip(all_x, all_y):
        key = str(int(i))
        if key in _cache:
            _cache[key].append(v)
        else:
            _cache[key] = [v]
        _cache_sorted = sorted(_cache.items(), key=lambda obj: int(obj[0]))
        xb = []
        yb_low = []
        yb_up = []
        for k, v in _cache_sorted:
            xb.append(int(k))
            yb_low.append(np.min(v))
            yb_up.append(np.max(v))
    return xb, yb_low, yb_up

#
# my empirical GA data
n, mye_emp = np.loadtxt('energies_empirical.dat', unpack=True)
n, mye_dft_lda = np.loadtxt('energies_parsec_lda.dat', unpack=True)
n, mye_dft_pbe = np.loadtxt('energies_parsec_pbe.dat', unpack=True)
n1, mye_dft_b3lyp = np.loadtxt('energies_nwchem_b3lyp.dat', unpack=True)
#
# Jones data BP - gradient xc
ncage_bp,  cage_bp = np.loadtxt('cage_BP_jones.dat', unpack=True)
nring_bp,  ring_bp = np.loadtxt('ring_BP_jones.dat', unpack=True)
ngraph_bp, graph_bp = np.loadtxt('graphitic_BP_jones.dat', unpack=True)
ndble_bp,  dble_bp = np.loadtxt('double_BP_jones.dat', unpack=True)
nchain_bp, chain_bp = np.loadtxt('chain_BP_jones.dat', unpack=True)
#
# Jones data LDA xc
ncage_ld,  cage_ld = np.loadtxt('cage_LD_jones.dat', unpack=True)
nring_ld,  ring_ld = np.loadtxt('ring_LD_jones.dat', unpack=True)
ngraph_ld, graph_ld = np.loadtxt('graphitic_LD_jones.dat', unpack=True)
ndble_ld,  dble_ld = np.loadtxt('double_LD_jones.dat', unpack=True)
nchain_ld, chain_ld = np.loadtxt('chain_LD_jones.dat', unpack=True)


# publishable = plotsettings.Set('Science')
mpl.rc('mathtext', fontset=u'stixsans')
mpl.rc('text', usetex=True)
mpl.rc('ytick', labelsize='large')
mpl.rc('xtick', labelsize='large')
# publishable.set_figsize(4, 2, aspect_ratio=1)

# ______________________________________________________________________
fig, ax = plt.subplots(1, 1)

l1, l2, l3, l4 = ax.plot(n, mye_emp, 'g-o',
                         n, mye_dft_lda, 'r-o',
                         n, mye_dft_pbe, 'r-s',
                         n1, mye_dft_b3lyp, 'b-o')

l1_ld, l2_ld, l3_ld, l4_ld, l5_ld = \
    ax.plot(ncage_ld,  -cage_ld,  'kD',
            nring_ld,  -ring_ld,  'ks',
            ngraph_ld, -graph_ld, 'kH',
            ndble_ld,  -dble_ld,  'k^',
            nchain_ld, -chain_ld, 'kv')

l1_bp, l2_bp, l3_bp, l4_bp, l5_bp = \
    ax.plot(ncage_bp,  -cage_bp,  'kD',
            nring_bp,  -ring_bp,  'ks',
            ngraph_bp, -graph_bp, 'kH',
            ndble_bp,  -dble_bp,  'k^',
            nchain_bp, -chain_bp, 'kv')

# -----------------------------------------------------------------
# create envelope to guide the eye
xlsd, ylsd_low, ylsd_up = create_envelope(ncage_ld, nring_ld,
                                          ngraph_ld, ndble_ld,
                                          nchain_ld,
                                          -cage_ld, -ring_ld,
                                          -graph_ld, -dble_ld,
                                          -chain_ld)

xgga, ygga_low, ygga_up = create_envelope(ncage_bp, nring_bp,
                                          ngraph_bp, ndble_bp,
                                          nchain_bp,
                                          -cage_bp, -ring_bp,
                                          -graph_bp, -dble_bp,
                                          -chain_bp)

ax.fill_between(xlsd, ylsd_low, ylsd_up, color=(0.75, 0.75, 0.75))
ax.fill_between(xgga, ygga_low, ygga_up, color=(0.75, 0.75, 0.75))
# -----------------------------------------------------------------


leg1 = plt.legend([l1, l2, l3, l4],
                  [r'cl\'assico (GA)', 'RS-DFT/LDA',
                  'RS-DFT/PBE', 'DFT/B3LYP/6-31G*'],
                  title='estudo presente',
                  loc=0)

leg2 = plt.legend([l1_ld, l2_ld, l3_ld, l4_ld, l5_ld],
                  ['gaiola', 'anel', 'grafeno',
                  'anel duplo', 'cadeia'],
                  title='Jones 1999\n(DFT-todos eletrons)',
                  loc=0,
                  numpoints=1)

ax.add_artist(leg1)

leg1.draggable()
leg2.draggable()

conn_d1 = "arc,angleA=0, armA=20, angleB=-90, armB=15, rad=7"
conn_d2 = "arc,angleA=90, armA=20, angleB=-90, armB=15, rad=7"
txt1 = ax.annotate('LSDA', xy=(29.0, -8.0), xycoords='data',
                   xytext=(-20, -20), textcoords='offset points',
                   horizontalalignment='left',
                   verticalalignment='bottom',
                   bbox=dict(boxstyle='round', fc='0.8'),
                   arrowprops=dict(arrowstyle="->",
                                   connectionstyle=conn_d1))
d1 = txt1.draggable()

txt2 = ax.annotate('GGA', xy=(31.0, -7.0), xycoords='data',
                   xytext=(-20, -20), textcoords='offset points',
                   horizontalalignment='left',
                   verticalalignment='bottom',
                   bbox=dict(boxstyle='round', fc='0.8'),
                   arrowprops=dict(arrowstyle="->",
                                   connectionstyle=conn_d2))
d2 = txt2.draggable()

ax.set_ylim(-10, -4.0)
ax.set_xlabel(r'N', fontdict=dict(size=14))
ax.set_ylabel(r'energia de coes\~ao (eV/\'atomo)', fontdict=dict(size=14))

plt.show()
fig.savefig('energies_comparison.pdf', dpi=300)
