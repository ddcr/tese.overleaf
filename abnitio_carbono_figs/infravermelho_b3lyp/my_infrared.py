#!/usr/bin/env python
# Sun, 14 Oct 12 [20:36]
# -*- coding: utf-8 -*-

"""Infrared intensities"""

import pickle
import os
import sys
from math import pi, sqrt, log
import numpy as np
#
from matplotlib import pyplot as plt
import my_lineid_plot
from matplotlib.offsetbox import (AnnotationBbox, OffsetImage,
                                  AnchoredOffsetbox, TextArea)
from matplotlib._png import read_png


from matplotlib.font_manager import FontProperties
from matplotlib.ticker import MaxNLocator
import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_pdf import PdfPages
import argparse
import errno

###############################################################################
from optparse import Option, OptionParser
from copy import copy


def waves_callback(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))


def check_directory(option, opt, value):
    if os.path.exists(value) and not os.path.isdir(value):
        raise argparse.ArgumentTypeError(
            "path '%s' already exists but is not a directory."
            % (value,))
    return value


class MyOption (Option):
    TYPES = Option.TYPES + ("valid_directory",)
    TYPE_CHECKER = copy(Option.TYPE_CHECKER)
    TYPE_CHECKER["valid_directory"] = check_directory

PROG = os.path.basename(sys.argv[0])
cmdline = OptionParser("%s [options] BASENAME" % PROG,
                       description="""
TO BE DONE LATER ... (TBDL)
""",
                       option_class=MyOption)


cmdline.add_option("-c", "--cluster",
                   metavar='CLUSTER FORMULA',
                   dest="cluster",
                   type="string",
                   help="Plot IR data of cluster")

cmdline.add_option('-p', '--peaks',
                   metavar="LIST",
                   type='string',
                   action='callback',
                   dest="peaks",
                   callback=waves_callback)

cmdline.add_option('-t', '--threshold',
                   metavar="FLOAT",
                   type='float',
                   action='store',
                   dest="threshold",
                   help="Select peaks above a given threshold")

cmdline.add_option('-f', '--fhwm',
                   metavar="FLOAT",
                   type='float',
                   action='store',
                   dest="fwhm",
                   default=4.0,
                   help="Convolve with Gaussian with specific" +
                   " FWHM [%default cm-1]")

cmdline.add_option('-i', '--image',
                   metavar="PNG FILE",
                   type='string',
                   action='store',
                   dest="molecule",
                   help="Insert molecule png image to the plot")

cmdline.add_option('-s', '--stacked',
                   action='store_true',
                   dest="stacked",
                   help="If this is selected we expect as " +
                   "argument a list of files")


(options, args) = cmdline.parse_args()


###############################################################################
class AnchoredText(AnchoredOffsetbox):
    def __init__(self, s, loc, pad=0.4, borderpad=0.5,
                 prop=None, frameon=True):

        self.txt = TextArea(s, minimumdescent=False)
        super(AnchoredText, self).__init__(loc, pad=pad, borderpad=borderpad,
                                           child=self.txt,
                                           prop=prop,
                                           frameon=frameon)


class DevNull:
    def write(self, string):
        pass

    def flush(self):
        pass

    def seek(self, offset, whence=0):
        return 0

    def tell(self):
        return 0

    def close(self):
        pass

devnull = DevNull()


class InfraRed(object):
    """
    """
    def __init__(self, *args, **kwargs):
        """
        Now lets read the peaks IR data
        """
        self.irdata = {}

        if kwargs.get('mode') is None:
            mode = 'r'
        else:
            mode = kwargs.get('mode')

        if len(args) > 0:
            filename = args[0]
            if isinstance(filename, str):
                self.open(filename, mode)

        if kwargs.get('filename') is not None:
            filename = kwargs.pop('filename')
            self.open(filename, mode)

    def open(self, filename, mode):
        """Opens the file.
        For internal use only.
        """
        self.fd = filename
        if mode == 'r':
            if isinstance(filename, str):
                self.fd = open(filename, 'rb')
            self.read_header()
        elif mode == 'a':
            """
            Cuidado aqui. Nao sei se o novo dictionary sera
            acrescentado ao ja existente ...
            """
            exists = True
            if isinstance(filename, str):
                exists = os.path.isfile(filename)
                if exists:
                    exists = os.path.getsize(filename) > 0
                if exists:
                    self.fd = open(filename, 'rb')
                    self.read_header()
                    self.fd.close()
                self.fd = open(filename, 'ab+')
        elif mode == 'w':
            if isinstance(filename, str):
                self.fd = open(filename, 'wb')
                if os.path.isfile(filename):
                    try:
                        os.rename(filename, filename + '.bak')
                    except WindowsError, e:
                        # this must run on Win only! Not atomic!
                        if e.errno != errno.EEXIST:
                            raise
                        os.unlink(filename + '.bak')
                        os.rename(filename, filename + '.bak')
            else:
                self.fd = devnull
        else:
            raise ValueError('mode must be "r", "w" or "a".')

    def read_header(self):
        self.fd.seek(0)
        try:
            if self.fd.read(len('NWChemInfraRedData')) != 'NWChemInfraRedData':
                raise IOError('This file does not contain Infrared data!')
            d = pickle.load(self.fd)
        except EOFError:
            raise EOFError('Bad pickle file.')
        self.irdata = d

    def plot_spectrum(self, clustname, ir_key=None,
                      line_wave=[], line_label=[],
                      start=300, end=2500):
        """
        """
        assert isinstance(clustname, str)
        if ir_key is None:
            print("Choose ir_key in [ir_au, ir_debye_ang, " +
                  "ir_KM_mol, ir_no_units]")
            return
        else:
            assert isinstance(ir_key, str)

        #
        # NOTA: the first six frequencies are translational plus rotational
        #       modes.
        #
        freq = np.array(self.irdata[clustname]['eig_cm'][6:])  # not scaled
        intensity = np.array(self.irdata[clustname][ir_key][6:])

        print freq
        # Normalize intensities into transmittances
        intensity_max = intensity.max()
        norm_intensity = intensity / intensity_max * 100.0

        # convolve singlet with gaussians
        start = min(300.0, freq[0])-50.0
        end = max(4000.0, freq[-1])+50.0

        start = freq[0] - 50.0
        end = freq[-1] + 50.0

        energies, spectrum = self.get_spectrum(freq,
                                               norm_intensity,
                                               start=start,
                                               end=end,
                                               fwhm=options.fwhm)

        # Normalization is probably screwed up, so renormalize the data
        smin = spectrum.min()
        smax = spectrum.max()
        spectrum = (spectrum - smin) / (smax - smin) * 100.0

        if options.threshold:
            """because we integrated the singlets convolved with gaussians
               in intervals of freq, close peaks sum to give a broader and
               higher peak.
            """
            spectrum_at_freq = np.interp(freq, energies, spectrum)
            line_wave = freq[np.where(spectrum_at_freq >= options.threshold)]
            line_label = ['%5.1f' % s for s in line_wave]

        # Now do the main plot
        fig, ax = my_lineid_plot.plot_line_ids(energies, spectrum,
                                               line_wave, line_label,
                                               box_axes_space=0.11)

        ax.set_ylim(top=105.0)

        # plot the line threshold
        ax.axhline(y=options.threshold, linestyle='--', color='k')

        # ====================================================================================
        # annotate molecule image with vibrations
        if options.molecule:
            arr_molecule = read_png(options.molecule)
            imagebox = OffsetImage(arr_molecule, zoom=0.4)

            ab = AnnotationBbox(imagebox, (0.85, 0.95),
                                xycoords='axes fraction',
                                frameon=False,
                                boxcoords="offset points",
                                # arrowprops=dict(arrowstyle="->",
                                #                connectionstyle="angle,angleA=0,angleB=90,rad=3")
                                )
            ax.add_artist(ab)
            ab.draggable()
        # ====================================================================================
        # Draw cluster label
        at = AnchoredText(clustname, loc=4, frameon=True)
        at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
        ax.add_artist(at)
        # ====================================================================================

        plt.xlabel(r'$\nu$ (cm$^{-1}$)')
        plt.ylabel(r'Absorbance (%)')
        plt.xlim(min(start, freq[0])-10.0, max(end, freq[-1])+10.0)

        plt.draw()
        plt.show()

# ############_--------------------WORK--------------------_#############

    def plot_stacked_spectra(self, ir_key=None):
        """
        """
        from itertools import cycle

        fmin = 2.3e13
        fmax = -fmin

        for k, v in self.irdata.items():
            x = v['eig_cm'][6:]
            y = v[ir_key][6:]
            fmin = min(fmin, np.min(x))
            fmax = max(fmax, np.max(x))

        # go second round to do convolution
        i = 0
        labels = []
        freqs = []
        ir = []
        # forma que eu achei para ordenar as chaves de self.irdata por ordem
        # crescente de acordo com a substring numerica da chave do tipo
        # CXX,  XX=3..25. COMPLICADO!!!
        for k in sorted(self.irdata.iterkeys(), key=lambda s: float(s[1:])):
            v = self.irdata[k]
            label = r"%s" % k
            labels.append(label)
            i += 1

            x = v['eig_cm'][6:]
            y = v[ir_key][6:]

            energy, spec = self.get_spectrum(x, y,
                                             start=fmin,
                                             end=3000.0,
                                             fwhm=options.fwhm)
            # Normalization is probably screwed up, so renormalize the data
            smin = spec.min()
            smax = spec.max()
            spec = (spec - smin) / (smax - smin) * 100.0

            freqs.append(energy)
            ir.append(spec)

        # ================= starts figure =================
        fig_width_pt = 595.28  # Get this from LaTeX using \showthe\columnwidth
        inches_per_pt = 1.0/72.27  # Convert pt to inches
        golden_mean = (np.sqrt(5)-1.0)/2.0  # Aesthetic ratio
        fig_width = fig_width_pt*inches_per_pt  # width in inches
        fig_height = fig_width*golden_mean  # height in inches
        fig_size = [fig_width, fig_height]

        pdf = PdfPages('image.pdf')
        plt.rcParams.update({'ps.usedistiller': 'xpdf'})
        fig = plt.figure(figsize=fig_size)
        gs0 = gridspec.GridSpec(1, 3)
        gs0.update(left=0.02, right=0.98, wspace=0.15, bottom=0.1)

        nplots = len(freqs)
        label_cycler = cycle(labels)

        nrow = 7
        l = 0
        for icol in range(3):
            if l > nplots-1:
                continue
            gs = gridspec.GridSpecFromSubplotSpec(nrow,
                                                  1,
                                                  subplot_spec=gs0[icol],
                                                  hspace=0)
            # --- first row in column
            ax1 = plt.Subplot(fig, gs[0, 0])
            fig.add_subplot(ax1)
            ax1.set_xlim(fmin, fmax)
            # -------------------------------------
            ax1.plot(freqs[l], ir[l], 'k-')
            ax1.set_ylim(top=1.2*np.max(ir))
            l += 1
            # -------------------------------------
            ax1.xaxis.set_major_locator(MaxNLocator(prune='both'))
            ax1.get_xaxis().set_visible(False)
            ax1.get_yaxis().set_visible(False)
            ax1.spines['bottom'].set_visible(False)
            lbl = next(label_cycler)
            ax1.text(0.85, 0.90, lbl, transform=ax1.transAxes,
                     fontsize=10, va='top')

            for jrow in range(1, nrow):
                ax = plt.Subplot(fig, gs[jrow, 0], sharex=ax1)
                if l > nplots - 1:
                    ax.get_xaxis().set_visible(True)
                    continue
                fig.add_subplot(ax)
                # -------------------------------------
                ax.plot(freqs[l], ir[l], 'k-')
                l += 1
                # -------------------------------------
                ax.set_ylim(top=1.2*np.max(ir))
                ax.yaxis.set_major_locator(MaxNLocator(prune='both'))
                ax.xaxis.set_major_locator(MaxNLocator(prune='both'))
                lbl = next(label_cycler)
                ax.text(0.85, 0.90, lbl,
                        transform=ax.transAxes,
                        fontsize=10, va='top')
                ax.get_yaxis().set_visible(False)
                if jrow < nrow-1:
                    ax.spines['bottom'].set_visible(False)
                    ax.spines['top'].set_visible(False)
                    ax.get_xaxis().set_visible(False)
                else:
                    ax.get_yaxis().set_visible(False)
                    ax.spines['top'].set_visible(False)

        plt.subplots_adjust()
        font = FontProperties(size=10)
        fig.text(0.5, 0.05,
                 r'Frequ\^encia (cm$^{-1}$)', ha='center', va='center',
                 fontproperties=font)
        fig.text(0.25, 0.5,
                 r'Intensidade (KM/mol)', ha='center', va='center',
                 rotation='vertical', fontproperties=font)

        pdf.savefig()
        pdf.close()
        plt.show()

# ############_--------------------WORK--------------------_#############

    def get_spectrum(self, frequencies, intensities, start=300, end=4000,
                     npts=None, fwhm=4, type='Gaussian'):
        """Get infrared spectrum. Limits can be 400-3500"""

        self.type = type.lower()
        assert self.type in ['gaussian', 'lorentzian']
        if not npts:
            npts = (end-start)/fwhm*10+1

        if type == 'lorentzian':
            intensities = intensities*fwhm*pi/2.
        else:
            sigma = fwhm/2./sqrt(2.*log(2.))

        # Make array with spectrum data
        energies = np.empty(npts, np.float)
        ediff = (end-start)/float(npts-1)
        energies = np.arange(start, end+ediff/2, ediff)

        # make sure that array spectrum[] has the same length as energies[]
        spectrum = np.empty(len(energies), np.float)

        for i, energy in enumerate(energies):
            energies[i] = energy
            if type == 'lorentzian':
                spectrum[i] = (intensities * 0.5 * fwhm / pi /
                               ((frequencies-energy)**2+0.25*fwhm**2)).sum()
            else:
                spectrum[i] = (intensities *
                               np.exp(-(frequencies - energy)**2/2./sigma**2)
                               ).sum()
        return [energies, spectrum]

    def write_spectra(self, out='ir-spectra.dat',
                      start=800, end=4000,
                      npts=None, fwhm=10,
                      type='Gaussian'):
        """Write out infrared spectrum to file."""

        energies, spectrum = self.get_spectrum(start, end, npts, fwhm, type)

        # Write out spectrum in file. First column is absolute intensities.
        # Second column is absorbance scaled so that data runs from 1 to 0
        spectrum2 = 1. - spectrum/spectrum.max()
        outdata = np.empty([len(energies), 3])
        outdata.T[0] = energies
        outdata.T[1] = spectrum
        outdata.T[2] = spectrum2
        fd = open(out, 'w')
        for row in outdata:
            fd.write('%.3f  %15.5e  %15.5e \n' % (row[0], row[1], row[2]))
        fd.close()

# ___________________________________________________________________________________
if __name__ == '__main__':
    """
    """
    IR = InfraRed(args[0])
    if options.cluster:
        if options.peaks is not None:
            line_wave = [float(x) for x in options.peaks]
            line_label = options.peaks
        else:
            line_wave = []
            line_label = []
        IR.plot_spectrum(options.cluster,
                         ir_key='ir_KM_mol',
                         line_wave=line_wave,
                         line_label=line_label)
    else:
        print 'do all the clusters'
        IR = InfraRed(args[0])
        IR.plot_stacked_spectra(ir_key='ir_KM_mol')
