#!/usr/bin/env python
# @Author: ddcr
# @Date:   2016-04-17 15:28:26
# @Last Modified by:   ddcr
# @Last Modified time: 2016-04-23 14:06:16

import numpy as np
import matplotlib.pyplot as plt
from pw_signal import scale, smooth
from scipy.signal import convolve
from cclib.parser import ccread
import logging


def lorentz_pwtools(M, std=1.0, sym=True):
    """Lorentz window (same as Cauchy function). Function skeleton stolen from
    scipy.signal.gaussian().

    The Lorentz function is

    .. math::

        L(x) = \frac{\Gamma}{(x-x_0)^2 + \Gamma^2}

    Here :math:`x_0 = 0` and `std` = :math:`\Gamma`.
    Some definitions use :math:`1/2\,\Gamma` instead of :math:`\Gamma`, but
    without 1/2 we get comparable peak width to Gaussians when using this
    window in convolutions, thus ``scipy.signal.gaussian(M, std=5)`` is similar
    to ``lorentz(M, std=5)``.

    Parameters
    ----------
    M : int
        number of points
    std : float
        spread parameter :math:`\Gamma`
    sym : bool, optional
        Description
    sym : bool

    Returns
    -------
    w : (M,)
    """
    if M < 1:
        return np.array([])
    if M == 1:
        return np.ones(1, dtype=float)
    odd = M % 2
    if not sym and not odd:
        M = M + 1
    n = np.arange(0, M) - (M - 1.0) / 2.0
    w = std / (n**2.0 + std**2.0)
    w /= w.max()
    if not sym and not odd:  # take out last element
        w = w[:-1]
    return w


def lorentz_kern(gamma, truncate=4.0):
    """Summary

    Parameters
    ----------
    gamma : TYPE
        Description
    truncate : float, optional
        Description
    sym : bool, optional
        Description

    Returns
    -------
    TYPE
        Description
    """
    sd = float(gamma)
    lw = int(truncate * sd + 0.5)
    w = [0.0] * (2 * lw + 1)
    w[lw] = 1.0
    sum = 1.0
    sd2 = sd * sd
    # calculate the kernel
    for ii in range(1, lw + 1):
        tmp = sd / (float(ii * ii) + sd2)
        w[lw + ii] = tmp
        w[lw - ii] = tmp
        sum += 2.0 * tmp
    for ii in range(2 * lw + 1):
        w[ii] /= sum
    return np.array(w)


data = ccread('PhCCCC_IR.out.bz2', loglevel=logging.ERROR, verbose=False)
if hasattr(data, 'vibirs'):
    vibdirs_tmp = data.vibirs
ir_line_int = vibdirs_tmp[vibdirs_tmp > 0].copy()
if hasattr(data, 'vibfreqs'):
    vibfreqs_tmp = data.vibfreqs
ir_line_nu = vibfreqs_tmp[vibdirs_tmp > 0].copy()

del vibdirs_tmp
del vibfreqs_tmp

# print(len(ir_line_nu), len(ir_line_int))
# sys.exit()

# Resampling irregularly spaced data to a regular grid in Python
# first we need to define the frequency step
delta_ir = np.min(np.diff(ir_line_nu))
ir_nu = np.arange(ir_line_nu[0], ir_line_nu[-1], delta_ir, dtype=np.float)
ir_bins, ir_nu_edges = np.histogram(ir_line_nu, bins=ir_nu)
# Check that bin edges enclose each line frequency
nonzeros, = np.nonzero(ir_bins)
ir_int = np.zeros_like(ir_nu, dtype=np.float)
ir_int[nonzeros] = ir_line_int
npoints = len(ir_int)
# scale for better representation
# fig = plt.figure()
# ax = fig.add_subplot(111)
# ax.vlines(ir_nu, [1.0], ir_int, color='b')
# ax.vlines(ir_line_nu, [0.0], [1.0], color='r')
# plt.show()

# now define the kernel
gamma = 3.0  # half FWHM
klen = int(200 * gamma)
klen = klen + 1 if klen % 2 == 0 else klen  # odd kernel
kernel = lorentz_pwtools(klen, std=gamma)

for nrand_fac in [0.2, 1.0]:
    fig = plt.figure()
    ax = fig.add_subplot(111)
    nrand = int(npoints * nrand_fac)
    # even nrand
    if nrand % 2 == 1:
        nrand += 1
    #
    # Sum of Lorentz functions at data points. This is the same as convolution
    # with a Lorentz function withOUT end point correction, valid if data `y`
    # is properly zero at both ends, else edge effects are visible: smoothed
    # data always goes to zero at both ends, even if original data doesn't. We
    # need to use a very wide kernel with at least 100*std b/c of long
    # Lorentz tails. Better 200*std to be safe.
    x = np.arange(len(ir_int))
    sig = np.zeros_like(ir_int)
    for xi, yi in enumerate(ir_int):
        sig += yi * gamma / ((x - xi)**2.0 + gamma**2.0)  # /np.pi
    sig = scale(sig)
    ax.plot(ir_nu, sig, label='sum')

    ax.vlines(ir_line_nu, [0], scale(ir_line_int))

    # convolution with wide kernel
    ax.plot(ir_nu, scale(convolve(ir_int, kernel, 'same') / kernel.sum()),
            label='conv, klen=%i' % klen)

    # Convolution with Lorentz function with end-point correction.
    for klen in [10*gamma, 100*gamma, 200*gamma]:
        klen = klen+1 if klen % 2 == 0 else klen  # odd kernel
        kernel = lorentz_pwtools(klen, std=gamma)
        ax.plot(ir_nu, scale(smooth(ir_int, kernel)), label='conv+egde, klen=%i' % klen)
        # ax.plot(smooth(y, kern), label='conv+egde, klen=%i' % klen)

    plt.title("npoints=%i, zero_paddind=%i" % (npoints, nrand))
    plt.legend()

plt.savefig('stacked.pdf', dpi=300)
# plt.show()
