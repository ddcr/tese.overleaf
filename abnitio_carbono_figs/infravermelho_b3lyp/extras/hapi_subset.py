#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: ddcr
# @Date:   2016-04-15 10:20:15
# @Last Modified by:   ddcr
# @Last Modified time: 2016-04-15 11:01:08
from numpy import sqrt, abs, exp, pi, log
from numpy import convolve
from numpy import floor, linspace


def arange_(lower, upper, step):
    npnt = floor((upper-lower)/step)+1
    upper_new = lower + step*(npnt-1)
    if abs((upper-upper_new)-step) < 1e-10:
        upper_new += step
        npnt += 1
    return linspace(lower, upper_new, npnt)


# spectral convolution
def SLIT_GAUSSIAN(x, g):
    """
    Instrumental (slit) function.
    B(x) = sqrt(ln(2)/pi)/Gamma*exp(-ln(2)*(x/Gamma)**2),
    where Gamma/2 is a gaussian half-width at half-maximum.
    """
    g /= 2
    return sqrt(log(2))/(sqrt(pi)*g)*exp(-log(2)*(x/g)**2)


# dispersion slit function
def SLIT_DISPERSION(x, g):
    """
    Instrumental (slit) function.
    B(x) = Gamma/pi/(x**2+Gamma**2),
    where Gamma/2 is a lorentzian half-width at half-maximum.
    """
    g /= 2
    return g/pi/(x**2+g**2)


def convolveSpectrum(Omega, CrossSection, Resolution=0.1, AF_wing=10.,
                     SlitFunction=SLIT_DISPERSION, Wavenumber=None):
    """
    INPUT PARAMETERS:
        Wavenumber/Omega:    wavenumber grid                     (required)
        CrossSection:  high-res cross section calculated on grid (required)
        Resolution:    instrumental resolution Gamma             (optional)
        AF_wing:       instrumental function wing                (optional)
        SlitFunction:  instrumental function for low-res spectra
                       calculation                               (optional)
    OUTPUT PARAMETERS:
        Wavenum: wavenumber grid
        CrossSection: low-res cross section calculated on grid
        i1: lower index in Omega input
        i2: higher index in Omega input
        slit: slit function calculated over grid [-AF_wing; AF_wing]
                with the step equal to instrumental resolution.
    ---
    DESCRIPTION:
        Produce a simulation of experimental spectrum via the convolution
        of a "dry" spectrum with an instrumental function.
        Instrumental function is provided as a parameter and
        is calculated in a grid with the width=AF_wing and step=Resolution.
    ---
    EXAMPLE OF USAGE:
        nu_,radi_,i,j,slit = convolveSpectrum(nu,radi,Resolution=2.0,
                                              AF_wing=10.0,
                                              SlitFunction=SLIT_MICHELSON)
    ---
    """
    # compatibility with older versions
    if Wavenumber:
        Omega = Wavenumber
    step = Omega[1]-Omega[0]
    if step >= Resolution:
        raise Exception('step must be less than resolution')
    # x = arange(-AF_wing,AF_wing+step,step)
    x = arange_(-AF_wing, AF_wing+step, step)  # fix
    slit = SlitFunction(x, Resolution)
    # FIXING THE BUG: normalize slit function
    slit /= sum(slit)*step  # simple normalization
    left_bnd = len(slit)/2
    right_bnd = len(Omega) - len(slit)/2
    # CrossSectionLowRes = convolve(CrossSection,slit,mode='valid')*step
    CrossSectionLowRes = convolve(CrossSection, slit, mode='same')*step
    # return Omega[left_bnd:right_bnd],CrossSectionLowRes,
    #        left_bnd,right_bnd,slit
    return Omega[left_bnd:right_bnd], CrossSectionLowRes[left_bnd:right_bnd],
    left_bnd, right_bnd, slit
