#!/usr/bin/env python
"""Summary
"""
#
# import hyperspy.api as hs
import numpy as np
from scipy.signal import convolve, fftconvolve
import matplotlib.pyplot as plt
import pickle
import hyperspy.api as hs
import matplotlib as mpl
from matplotlib.widgets import Slider, Button
from cclib.parser import ccread
import logging


def pad_zeros(arr, axis=0, where='end', nadd=None,
              upto=None, tonext=None,
              tonext_min=None):
    """Pad an nd-array with zeros. Default is to append an array
    of zeros of the same shape as `arr` to arr's end along `axis`.

    Parameters
    ----------
    arr : nd array
    axis : the axis along which to pad
    where : string {'end', 'start'}, pad at the end ("append to array") or
        start ("prepend to array") of `axis`
    nadd : number of items to padd (i.e. nadd=3 means padd w/ 3 zeros in case
        of an 1d array)
    upto : pad until arr.shape[axis] == upto
    tonext : bool, pad up to the next power of two (pad so that the padded
        array has a length of power of two)
    tonext_min : int, when using 'tonext', pad the array to the next possible
        power of two for which the resulting array length along 'axis' is at
        least 'tonext_min'; the default is tonext_min = arr.shape[axis]
    Use only one of nadd, upto, tonext.

    Returns
    -------
    padded array

    Examples
    --------
    >>> # 1d
    >>> pad_zeros(a)
    array([1, 2, 3, 0, 0, 0])
    >>> pad_zeros(a, nadd=3)
    array([1, 2, 3, 0, 0, 0])
    >>> pad_zeros(a, upto=6)
    array([1, 2, 3, 0, 0, 0])
    >>> pad_zeros(a, nadd=1)
    array([1, 2, 3, 0])
    >>> pad_zeros(a, nadd=1, where='start')
    array([0, 1, 2, 3])
    >>> # 2d
    >>> a=arange(9).reshape(3,3)
    >>> pad_zeros(a, nadd=1, axis=0)
    array([[0, 1, 2],
           [3, 4, 5],
           [6, 7, 8],
           [0, 0, 0]])
    >>> pad_zeros(a, nadd=1, axis=1)
    array([[0, 1, 2, 0],
           [3, 4, 5, 0],
           [6, 7, 8, 0]])
    >>> # up to next power of two
    >>> 2**arange(10)
    array([  1,   2,   4,   8,  16,  32,  64, 128, 256, 512])
    >>> pydos.pad_zeros(arange(9), tonext=True).shape
    (16,)

    Raises
    ------
    StandardError
        Description
    """

    if tonext is False:
        tonext = None
    lst = [nadd, upto, tonext]
    assert lst.count(None) in [2, 3], "`nadd`, `upto` and `tonext` " +\
                                      "must be all None or only one " +\
                                      "of them not None"
    if nadd is None:
        if upto is None:
            if (tonext is None) or (not tonext):
                # default
                nadd = arr.shape[axis]
            else:
                tonext_min = arr.shape[axis] if (tonext_min is None) else tonext_min
                # beware of int overflows starting w/ 2**arange(64), but we
                # will never have such long arrays anyway
                two_powers = 2**np.arange(30)
                assert tonext_min <= two_powers[-1], "tonext_min exceeds " +\
                    "max power of 2"
                power = two_powers[np.searchsorted(two_powers,
                                                   tonext_min)]
                nadd = power - arr.shape[axis]
        else:
            nadd = upto - arr.shape[axis]
    if nadd == 0:
        return arr
    add_shape = list(arr.shape)
    add_shape[axis] = nadd
    add_shape = tuple(add_shape)
    if where == 'end':
        return np.concatenate((arr, np.zeros(add_shape, dtype=arr.dtype)),
                              axis=axis)
    elif where == 'start':
        return np.concatenate((np.zeros(add_shape, dtype=arr.dtype), arr),
                              axis=axis)
    else:
        raise StandardError("illegal `where` arg: %s" % where)


def lorentz(M, std=1.0, sym=True):
    """Lorentz window (same as Cauchy function). Function skeleton stolen from
    scipy.signal.gaussian().

    The Lorentz function is

    .. math::

        L(x) = \frac{\Gamma}{(x-x_0)^2 + \Gamma^2}

    Here :math:`x_0 = 0` and `std` = :math:`\Gamma`.
    Some definitions use :math:`1/2\,\Gamma` instead of :math:`\Gamma`, but
    without 1/2 we get comparable peak width to Gaussians when using this
    window in convolutions, thus ``scipy.signal.gaussian(M, std=5)`` is similar
    to ``lorentz(M, std=5)``.

    Parameters
    ----------
    M : int
        number of points
    std : float
        spread parameter :math:`\Gamma`
    sym : bool, optional
        Description
    sym : bool

    Returns
    -------
    w : (M,)
    """
    if M < 1:
        return np.array([])
    if M == 1:
        return np.ones(1, dtype=float)
    odd = M % 2
    if not sym and not odd:
        M = M + 1
    n = np.arange(0, M) - (M - 1.0) / 2.0
    w = std / (n**2.0 + std**2.0)
    w /= w.max()
    if not sym and not odd:
        w = w[:-1]
    return w


def smooth(data, kern, axis=0, edge='m', norm=True):
    """Smooth `data` by convolution with a kernel `kern`.

    Uses scipy.signal.fftconvolve().

    Note that due to edge effect handling (padding) and kernal normalization,
    the convolution identity convolve(data,kern) == convolve(kern,data) doesn't
    apply here. We always return an array of ``data.shape``.

    Parameters
    ----------
    data : nd array
        The data to smooth. Example: 1d (N,) or (N,natoms,3)
        for trajectory
    kern : nd array
        Convolution kernel. Example: 1d (M,) or (M,1,1)
        for trajectory
    axis : int
        Axis along which to do the smoothing. That is actually not needed for
        the convolution ``fftconvolve(data, kern)`` but is used for padding the
        data along `axis` to handle edge effects before convolution.
    edge : str
        Method for edge effect handling.
            | 'm' : pad with mirror signal
            | 'c' : pad with constant values (i.e. ``data[0]`` and
            |       ``data[-1]`` in the 1d case)
    norm : bool
        Normalize kernel. Default is True. This assures that the smoothed
        signal lies within the data. Note that this is not True for kernels
        with very big spread (i.e. ``hann(N*10)`` or ``gaussian(N/2,
        std=N*10)``. Then the kernel is effectively a constant.

    Returns
    -------
    ret : data.shape
        Convolved signal.

    Examples
    --------
    >>> from pwtools.signal import welch
    >>> from numpy.random import rand
    >>> x = linspace(0,2*pi,500); a=cos(x)+rand(500)
    >>> plot(a, color='0.7')
    >>> k=scipy.signal.hann(21)
    >>> plot(signal.smooth(a,k), 'r', label='hann')
    >>> k=scipy.signal.gaussian(21, 3)
    >>> plot(signal.smooth(a,k), 'g', label='gauss')
    >>> k=welch(21)
    >>> plot(signal.smooth(a,k), 'y', label='welch')
    >>> legend()
    >>> # odd kernel [0,1,0] reproduces data exactly, i.e. convolution with
    >>> # delta peak
    >>> figure(); title('smooth with delta [0,1,0]')
    >>> x=linspace(0,2*pi,15); k=scipy.signal.hann(3)
    >>> plot(cos(x))
    >>> plot(signal.smooth(cos(x),k), 'r')
    >>> legend()
    >>> # edge effects with normal convolution
    >>> figure(); title('edge effects')
    >>> x=rand(20)+10; k=scipy.signal.hann(11);
    >>> plot(x); plot(signal.smooth(x,k),label="smooth");
    >>> plot(scipy.signal.convolve(x,k/k.sum(),'same'), label='convolve')
    >>> legend()
    >>> # edge effect methods
    >>> figure(); title('edge effect methods')
    >>> x=rand(20)+10; k=scipy.signal.hann(20);
    >>> plot(x); plot(signal.smooth(x,k,edge='m'),label="edge='m'");
    >>> plot(signal.smooth(x,k,edge='c'),label="edge='c'");
    >>> legend()
    >>> # smooth a trajectory of atomic coordinates
    >>> figure(); title('trajectory')
    >>> x = linspace(0,2*pi,500)
    >>> a = rand(500,2,3) # (nstep, natoms, 3)
    >>> a[:,0,:] += cos(x)[:,None]
    >>> a[:,1,:] += sin(x)[:,None]
    >>> k=scipy.signal.hann(21)[:,None,None]
    >>> y = signal.smooth(a,k)
    >>> plot(a[:,0,0], color='0.7'); plot(y[:,0,0],'b',
    ...                                   label='atom1 x')
    >>> plot(a[:,1,0], color='0.7'); plot(y[:,1,0],'r',
    ...                                   label='atom2 x')
    >>> legend()

    References
    ----------
    [1] http://wiki.scipy.org/Cookbook/SignalSmooth

    See Also
    --------
    :func:`welch`
    :func:`lorentz`

    Notes
    -----

    Kernels:

    Even kernels result in shifted signals, odd kernels are better.
    However, for N >> M, it doesn't make a difference really.

    Usual kernels (window functions) are created by e.g.
    ``scipy.signal.hann(M)``. For ``kern=scipy.signal.gaussian(M,
    std)``, two values are needed, namely `M` and `std`, where  `M`
    determines the number of points calculated for the convolution kernel, as
    in the other cases. But what is actually important is `std`, which
    determines the "used width" of the gaussian. Say we use N=100
    and M=50. That would be a massively wide window and we would
    smooth away all details. OTOH, using ``gaussian(50,3)`` would generate a
    kernel with the same number `M` of data points, but the gauss peak which is
    effectively used for convolution is much smaller. For ``gaussian()``,
    `M` should be bigger then `std`. The convolved signal will converge
    with increasing `M`. Good values are `M=6*std` and bigger. For
    :func:`lorentz`, much wider kernels are needed such as `M=100*std` b/c
    of the long tails of the Lorentz function. Testing is mandatory!

    Edge effects:

    We use padding of the signal with ``M=len(kern)`` values at both ends such
    that the convolution with `kern` doesn't zero the `data` at the signal
    edges. We have two methods. `edge='m'`: padd with the signal mirrored at 0
    and -1 or `edge='c'`: use the constant values ``data[0]`` and ``data[-1]``.
    Many more of these variants may be thought of. The choice of how to extend
    the data essentially involves an assumption about how the signal *would*
    continue, which is signal-dependent. In practice, we usually have ``M <<
    N`` (e.g. ``scipy.signal.hann(M)``) or ``std << N``
    (``scipy.signal.gaussian(M, std``). Then, both methods are identical in the
    middle and show only very small differences at the edges. Essentially, edge
    effect handling shall only ensure that the smoothed signal doesn't go to
    zero and that must be independent of the method, which is the case.

    Memory:

    For big data, fftconvolve() can easily eat up all your memory, for
    example::

    >>> # assume axis=0 is the axis along which to convolve
    >>> arr = ones((1e5,200,3))
    >>> kern = scipy.signal.hann(101)
    >>> ret = scipy.signal.fftconvolve(arr, kern[:,None,None])

    Then it is better to loop over some or all of the remaing dimensions::

    >>> ret = np.empty_like(arr)
    >>> for jj in range(arr.shape[1]):
    >>>     ret[:,jj,:] = smooth(arr[:,jj,:], kern[:,None])

    or::

    >>> for jj in range(arr.shape[1]):
    >>>     for kk in range(arr.shape[2]):
    >>>         ret[:,jj,kk] = smooth(arr[:,jj,kk], kern)

    The size of the chunk over which you explicitely loop depends on the data
    of course.

    Raises
    ------
    StandardError
        Description
    """
    N = data.shape[axis]
    M = kern.shape[axis]
    if edge == 'm':
        npad = min(M, N)
        sleft = slice(npad, 0, -1)
        sright = slice(-2, -(npad + 2), -1)
        dleft = slicetake(data, sl=sleft, axis=axis)
        dright = slicetake(data, sl=sright, axis=axis)
        assert dleft.shape == dright.shape
        K = dleft.shape[axis]
        if K < M:
            dleft = pad_zeros(dleft, axis=axis, where='start', nadd=M - K)
            dright = pad_zeros(dright, axis=axis, where='end', nadd=M - K)
    elif edge == 'c':
        sl = [slice(None)] * data.ndim
        sl[axis] = None
        dleft = np.repeat(slicetake(data, sl=0, axis=axis)[sl], M,
                          axis=axis)
        dright = np.repeat(slicetake(data, sl=-1, axis=axis)[sl], M,
                           axis=axis)
        assert dleft.shape == dright.shape
        # 1d special case: (M,1) -> (M,)
        if data.ndim == 1 and dleft.ndim == 2 and dleft.shape[1] == 1:
            dleft = dleft[:, 0]
            dright = dright[:, 0]
    else:
        raise StandardError("unknown value for edge")
    sig = np.concatenate((dleft, data, dright), axis=axis)
    kk = kern / float(kern.sum()) if norm else kern
    ret = fftconvolve(sig, kk, 'valid')
    assert ret.shape[axis] == N + M + 1, "unexpected convolve result shape"
    del sig
    if M % 2 == 0:
        # sl = slice(M/2+1,-(M/2)) # even kernel, shift result to left
        sl = slice(M / 2, - (M / 2) - 1)  # even kernel, shift result to right
    else:
        sl = slice(M / 2 + 1, - (M / 2) - 1)
    ret = slicetake(ret, sl=sl, axis=axis)
    assert ret.shape == data.shape, "ups, ret.shape (%s)!= data.shape (%s)" %\
                                    (ret.shape, data.shape)
    return ret


def scale(x, copy=True):
    """Scale `x` to unity.

    Subtract min and divide by (max-min).

    Parameters
    ----------
    x : TYPE
        Description
    copy : bool
        copy `x` before scaling
    x : array_like

    Returns
    -------
    x_scaled
    """
    xx = x.copy() if copy else x
    xx = xx - xx.min()
    xx /= xx.max()
    return xx


def slicetake(a, sl, axis=None, copy=False):
    """The equivalent of numpy.take(a, ..., axis=<axis>), but accepts slice
    objects instead of an index array. Also by default, it returns a *view* and
    no copy.

    Parameters
    ----------
    a : TYPE
        Description
    sl : slice object, list or tuple of slice objects
        axis=<int>
            one slice object for *that* axis
        axis=None
            `sl` is a list or tuple of slice objects, one for each axis.
            It must index the whole array, i.e. len(sl) == len(a.shape).
    axis : None, optional
        Description
    copy : bool, optional
        Description
    a : numpy ndarray
    axis : {None, int}
    copy : bool, return a copy instead of a view

    Returns
    -------
    A view into `a` or copy of a slice of `a`.

    Examples
    --------
    >>> from numpy import s_
    >>> a = np.random.rand(20,20,20)
    >>> b1 = a[:,:,10:]
    >>> # single slice for axis 2
    >>> b2 = slicetake(a, s_[10:], axis=2)
    >>> # tuple of slice objects
    >>> b3 = slicetake(a, s_[:,:,10:])
    >>> (b2 == b1).all()
    True
    >>> (b3 == b1).all()
    True
    >>> # simple extraction too, sl = integer
    >>> (a[...,5] == slicetake(a, 5, axis=-1))
    True
    """
    # The long story
    # --------------
    #
    # 1) Why do we need that:
    #
    # # no problem
    # a[5:10:2]
    #
    # # the same, more general
    # sl = slice(5,10,2)
    # a[sl]
    #
    # But we want to:
    #  - Define (type in) a slice object only once.
    #  - Take the slice of different arrays along different axes.
    # Since numpy.take() and a.take() don't handle slice objects, one would
    # have to use direct slicing and pay attention to the shape of the array:
    #
    #     a[sl], b[:,:,sl,:], etc ...
    #
    # We want to use an 'axis' keyword instead. np.r_() generates index arrays
    # from slice objects (e.g r_[1:5] == r_[s_[1:5] ==r_[slice(1,5,None)]).
    # Since we need index arrays for numpy.take(), maybe we can use that? Like
    # so:
    #
    #     a.take(r_[sl], axis=0)
    #     b.take(r_[sl], axis=2)
    #
    # Here we have what we want: slice object + axis kwarg.
    # But r_[slice(...)] does not work for all slice types. E.g. not for
    #
    #     r_[s_[::5]] == r_[slice(None, None, 5)] == array([], dtype=int32)
    #     r_[::5]                                 == array([], dtype=int32)
    #     r_[s_[1:]]  == r_[slice(1, None, None)] == array([0])
    #     r_[1:]
    #         ValueError: dimensions too large.
    #
    # The returned index arrays are wrong (or we even get an exception).
    # The reason is given below.
    # Bottom line: We need this function.
    #
    # The reason for r_[slice(...)] gererating sometimes wrong index arrays is
    # that s_ translates a fancy index (1:, ::5, 1:10:2, ...) to a slice
    # object. This *always* works. But since take() accepts only index arrays,
    # we use r_[s_[<fancy_index>]], where r_ translates the slice object
    # prodced by s_ to an index array. THAT works only if start and stop of the
    # slice are known. r_ has no way of knowing the dimensions of the array to
    # be sliced and so it can't transform a slice object into a correct index
    # array in case of slice(<number>, None, None) or slice(None, None,
    # <number>).
    #
    # 2) Slice vs. copy
    #
    # numpy.take(a, array([0,1,2,3])) or a[array([0,1,2,3])] return a copy of
    # `a` b/c that's "fancy indexing". But a[slice(0,4,None)], which is the
    # same as indexing (slicing) a[:4], return *views*.

    if axis is None:
        slices = sl
    else:
        # Note that these are equivalent:
        #   a[:]
        #   a[s_[:]]
        #   a[slice(None)]
        #   a[slice(None, None, None)]
        #   a[slice(0, None, None)]
        slices = [slice(None)] * a.ndim
        slices[axis] = sl
    # a[...] can take a tuple or list of slice objects
    # a[x:y:z, i:j:k] is the same as
    # a[(slice(x,y,z), slice(i,j,k))] == a[[slice(x,y,z), slice(i,j,k)]]
    if copy:
        return a[slices].copy()
    else:
        return a[slices]


def gaussian_kern(sigma, truncate=4.0):
    """Summary

    Parameters
    ----------
    sigma : TYPE
        Description
    truncate : float, optional
        Description

    Returns
    -------
    TYPE
        Description
    """

    sd = float(sigma)
    lw = int(truncate * sd + 0.5)
    w = [0.0] * (2 * lw + 1)
    w[lw] = 1.0
    sum = 1.0
    sd2 = sd * sd
    # calculate the kernel
    for ii in range(1, lw + 1):
        tmp = np.exp(-0.5 * float(ii * ii) / sd2)
        w[lw + ii] = tmp
        w[lw - ii] = tmp
        sum += 2.0 * tmp
    for ii in range(2 * lw + 1):
        w[ii] /= sum
    return w


def lorentz_kern(gamma, truncate=4.0):
    """Summary

    Parameters
    ----------
    gamma : TYPE
        Description
    truncate : float, optional
        Description
    sym : bool, optional
        Description

    Returns
    -------
    TYPE
        Description
    """
    sd = float(gamma)
    lw = int(truncate * sd + 0.5)
    w = [0.0] * (2 * lw + 1)
    w[lw] = 1.0
    sum = 1.0
    sd2 = sd * sd
    # calculate the kernel
    for ii in range(1, lw + 1):
        tmp = sd / (float(ii * ii) + sd2)
        w[lw + ii] = tmp
        w[lw - ii] = tmp
        sum += 2.0 * tmp
    for ii in range(2 * lw + 1):
        w[ii] /= sum
    return w


def kernel_tests(kernel, *args, **kwargs):
    """test with of gaussian"""
    std0, = args
    tr0 = kwargs['truncate']
    kern = kernel(*args, **kwargs)
    klen = len(kern)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.autoscale_view(True, True, True)
    plt.subplots_adjust(bottom=0.25)
    l, = plt.plot(range(klen), kern, 'go')
    vline_collection = plt.vlines(range(klen), [0], kern)
    ax.set_title('Kernel has {0} points'.format(klen))

    axcolor = 'lightgoldenrodyellow'
    std_axis = plt.axes([0.25, 0.1, 0.65, 0.03], axisbg=axcolor)
    tr_axis = plt.axes([0.25, 0.15, 0.65, 0.03], axisbg=axcolor)

    std = Slider(std_axis, 'Std', 1.0, 10.0, valinit=std0)
    tr = Slider(tr_axis, 'Truncate', 1.0, 100.0, valinit=tr0)

    def update_kern(val):
        std_val = std.val
        tr_val = tr.val
        y_val = gaussian_kern(std_val, tr_val)
        klen_val = len(y_val)
        x_val = range(klen_val)
        # update points
        l.set_ydata(y_val)
        l.set_xdata(x_val)
        # update vertical line collection
        verts = [((thisx, thisymin), (thisx, thisymax))
                 for thisx, thisymin, thisymax
                 in zip(x_val, [0] * klen_val, y_val)]
        vline_collection.set_verts(verts)

        ax.set_title('Kernel has {0} points'.format(klen_val))
        ax.relim()
        ax.autoscale_view()

        fig.canvas.draw_idle()

    std.on_changed(update_kern)
    tr.on_changed(update_kern)

    reset_ax = plt.axes([0.8, 0.025, 0.1, 0.04])
    button = Button(reset_ax, 'Reset', color=axcolor, hovercolor='0.975')

    def reset(event):
        std.reset()
        tr.reset()
    button.on_clicked(reset)

    plt.show()


def lorentz_pwtools(M, std=1.0, sym=True):
    """Lorentz window (same as Cauchy function). Function skeleton stolen from
    scipy.signal.gaussian().

    The Lorentz function is

    .. math::

        L(x) = \frac{\Gamma}{(x-x_0)^2 + \Gamma^2}

    Here :math:`x_0 = 0` and `std` = :math:`\Gamma`.
    Some definitions use :math:`1/2\,\Gamma` instead of :math:`\Gamma`, but
    without 1/2 we get comparable peak width to Gaussians when using this
    window in convolutions, thus ``scipy.signal.gaussian(M, std=5)`` is similar
    to ``lorentz(M, std=5)``.

    Parameters
    ----------
    M : int
        number of points
    std : float
        spread parameter :math:`\Gamma`
    sym : bool, optional
        Description
    sym : bool

    Returns
    -------
    w : (M,)
    """
    if M < 1:
        return np.array([])
    if M == 1:
        return np.ones(1, dtype=float)
    odd = M % 2
    if not sym and not odd:
        M = M + 1
    n = np.arange(0, M) - (M - 1.0) / 2.0
    w = std / (n**2.0 + std**2.0)
    w /= w.max()
    if not sym and not odd:  # take out last element
        w = w[:-1]
    return w


def convolve_tests():
    """test"""
    data = ccread('PhCCCC_IR.out', loglevel=logging.ERROR, verbose=False)
    if hasattr(data, 'vibirs'):
        vibdirs_tmp = data.vibirs
    ir_line_int = vibdirs_tmp[vibdirs_tmp > 0].copy()
    if hasattr(data, 'vibfreqs'):
        vibfreqs_tmp = data.vibfreqs
    ir_line_nu = vibfreqs_tmp[vibdirs_tmp > 0].copy()

    del vibdirs_tmp
    del vibfreqs_tmp

    # Resampling irregularly spaced data to a regular grid in Python
    # first we need to define the frequency step
    delta_ir = np.min(np.diff(ir_line_nu))
    ir_nu = np.arange(ir_line_nu[0], ir_line_nu[-1], delta_ir, dtype=np.float)
    ir_bins, ir_nu_edges = np.histogram(ir_line_nu, bins=ir_nu)
    # Check that bin edges enclose each line frequency
    nonzeros, = np.nonzero(ir_bins)
    ir_int = np.zeros_like(ir_nu, dtype=np.float)
    ir_int[nonzeros] = ir_line_int
    npoints = len(ir_int)

    # now define the kernel
    gamma = 3.0  # half FWHM
    klen = int(200 * gamma)
    klen = klen + 1 if klen % 2 == 0 else klen  # odd kernel
    kernel = lorentz_pwtools(klen, std=gamma)

    for nrand_fac in [0.2, 1.0]:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        nrand = int(npoints * nrand_fac)
        # even nrand
        if nrand % 2 == 1:
            nrand += 1
        #
        # Sum of Lorentz functions at data points. This is the same
        # as convolution with a Lorentz function withOUT end point
        # correction, valid if data `ir_int` is properly zero at both
        # ends, else edge effects are visible: smoothed data always
        # goes to zero at both ends, even if original data doesn't. We
        # need to use a very wide kernel with at least 100*std b/c
        # of long Lorentz tails. Better 200*std to be safe.
        #
        x = np.arange(len(ir_int))
        sig = np.zeros_like(ir_int)
        for xi, yi in enumerate(ir_int):
            sig += yi * gamma / ((x - xi)**2.0 + gamma**2.0)  # /np.pi
        sig = scale(sig)
        ax.plot(ir_nu, sig, label='sum')

        ax.vlines(ir_line_nu, [0], scale(ir_line_int))

        # convolution with wide kernel
        ax.plot(ir_nu, scale(convolve(ir_int, kernel, 'same') / kernel.sum()),
                label='conv, klen=%i' % klen)

        # Convolution with Lorentz function with end-point correction.
        for klen in [10 * gamma, 100 * gamma, 200 * gamma]:
            klen = klen + 1 if klen % 2 == 0 else klen  # odd kernel
            kernel = lorentz_pwtools(klen, std=gamma)
            ax.plot(ir_nu, scale(smooth(ir_int, kernel)),
                    label='conv+egde, klen=%i' % klen)
            # ax.plot(smooth(y, kern), label='conv+egde, klen=%i' % klen)

        plt.title("npoints=%i, zero_paddind=%i" % (npoints, nrand))
        plt.legend()

    plt.show()


def thesis_plot():
    """Get epsilon curve: spectra folded with lorentzian profile"""
    irdata_filename = 'all_clusters.IR.pick'
    irdata = dict()

    fd = open(irdata_filename, 'rb')
    fd.seek(0)
    hdr_name = b'NWChemInfraRedData'
    try:
        if fd.read(len(hdr_name)) != hdr_name:
            raise IOError('No infrared data from {}'.format(irdata_filename))
        irdata = pickle.load(fd)
    except EOFError:
        raise EOFError('{}: pickle file damaged!'.format(irdata_filename))

    # Define the kernel
    gamma = 5.0  # half FWHM: 3 cm-1
    klen = int(200 * gamma)
    klen = klen + 1 if klen % 2 == 0 else klen  # odd kernel
    kernel = lorentz_pwtools(klen, std=gamma)

    # Get limits in frequency. Ignore the first 6 values as
    # they are pure rotational plus translational modes
    ir_nu_low = np.array([v['eig_cm'][6] for k, v in irdata.items()]).min()
    ir_nu_upp = np.array([v['eig_cm'][-1] for k, v in irdata.items()]).max()

    mpl.rc('font', family='serif')
    mpl.rc('mathtext', fontset='custom')
    mpl.rc('text', usetex=True)
    mpl.rc('path', simplify=True)

    fig, axarr = plt.subplots(nrows=1, ncols=3,
                              sharex=True)
    spec_no = 1  # spectrum number
    subplt_no = 0  # subplot column
    spectra = [[]]

    for c in range(3, 26):

        # get infrared line data from several abnitio calculations
        try:
            irdata_cluster = irdata['C{}'.format(c)]
        except KeyError:
            print('Data loading failed'
                  ' for cluster C{0} (plt#{1})'.format(c, subplt_no))
            continue
        ir_line_int = np.asarray(irdata_cluster['ir_KM_mol'][6:])
        ir_line_nu = np.asarray(irdata_cluster['eig_cm'][6:])

        # Resample irregularly spaced data to a regular grid in Python
        # Define first the frequency step
        delta_nu = gamma / 6.0
        ir_nu = np.arange(ir_nu_low, ir_nu_upp, delta_nu, dtype=np.float)
        count = 0
        while count < 5:
            ir_bins, ir_bin_edges = np.histogram(ir_line_nu,
                                                 bins=ir_nu)
            if np.any(ir_bins > 1):
                print('C{0} (plt#{1}): decrease'
                      ' delta_nu in binning'.format(c, subplt_no))
                delta_nu /= 2.0
                ir_nu = np.arange(ir_nu_low, ir_nu_upp,
                                  delta_nu, dtype=np.float)
            else:
                print('C{0} (plt#{1}): binning OK'.format(c, subplt_no))
                break
            count += 1

        # put lines of array `ir_line_int` in the right bins of `ir_int`
        nonzero_ir_bins, = np.nonzero(ir_bins)
        ir_int = np.zeros_like(ir_nu, dtype=np.float)
        ir_int[nonzero_ir_bins] = ir_line_int

        # broaden spectrum with lorentzian line profile and save the signal
        # in hyperspy class Spectrum
        sig = scale(smooth(ir_int, kernel))
        metadata = {'General': {'title': r'C$_{{{}}}$'.format(c)}}
        s = hs.signals.Spectrum(sig, metadata=metadata)
        s.axes_manager[0].scale = delta_nu
        s.axes_manager[0].offset = ir_nu_low
        s.axes_manager[0].name = r'Frequ\^encia'
        s.axes_manager[0].units = r'cm$^{-1}$'

        spectra[subplt_no].append(s)

        if spec_no % 7 == 0 and subplt_no < 2:  # move to next subplot
            subplt_no += 1
            spectra.append([])
            print('Next cluster starts in subplot {0}'.format(subplt_no))
            print('=' * 80)

        spec_no += 1

    for subplt_no in range(len(spectra)):
        ax = axarr[subplt_no]
        hs.plot.plot_spectra(spectra[subplt_no],
                             style='cascade',
                             padding=-1.0,
                             color='k',
                             legend=None,
                             ax=ax,
                             fig=fig)
        ax.set_xlim((ir_nu_low, 2500.0))
        # annotate spectra included in each subplot
        # remenber that spectra[subplt_no] should be
        # run in reverse order (see the above keyword 'paddind')
        subplt_step = 1.0 / float(len(spectra[subplt_no]))
        for i, s in enumerate(spectra[subplt_no][::-1]):
            stext = s.metadata.General.title
            ax.annotate(stext,
                        xy=(2400.0, 0.0),
                        xytext=(0.85, (float(i)+0.5)*subplt_step),
                        textcoords='axes fraction',
                        arrowprops=None,
                        horizontalalignment='left',
                        verticalalignment='bottom')

    axarr[0].set_ylabel('Intensidade normalizada')

    fig.subplots_adjust(bottom=0.05, right=0.95, wspace=0.01)
    plt.tight_layout()
    # plt.show()
    plt.savefig('stacked_ir.pdf', dpi=300)


if __name__ == '__main__':

    # convolve_tests()
    thesis_plot()
