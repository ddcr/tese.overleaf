#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
#
#
# Copyright (C) 2015 Domingos Rodrigues <ddcr@lcc.ufmg.br>
#
# Created: Mon Dec 28 13:38:55 2015 (BRST)
#
# $Id$
#

__id__ = "$Id:$"
__revision__ = "$Revision:$"

from eps_writer import *
import os
import cairosvg

svg_files = ["C6.ga.bond.svg", "C7.ga.bond.svg", "C8.ga.bond.svg",
             "C9.ga.bond.svg", "C10.ga.bond.svg", "C11.ga.bond.svg",
             "C6.lda.bond.svg", "C7.lda.bond.svg", "C8.lda.bond.svg",
             "C9.lda.bond.svg", "C10.lda.bond.svg", "C11.lda.bond.svg",
             "C6.pbe.bond.svg", "C7.pbe.bond.svg", "C8.pbe.bond.svg",
             "C9.pbe.bond.svg", "C10.pbe.bond.svg", "C11.pbe.bond.svg",
             "C6.b3lyp.bond.svg", "C7.b3lyp.bond.svg", "C8.b3lyp.bond.svg",
             "C9.b3lyp.bond.svg", "C10.b3lyp.bond.svg", "C11.b3lyp.bond.svg"]

mp = multiplot(6,4, figsize=(5, 5),
               svg_files=svg_files, bare_axes=True , margins=(0.0,0.0),
               ylabels=["GA", "RSDFT/LDA", "RSDFT/PBE", "B3LYP/6-31G*"])
#
# add titles on top
#
text_opts = [pyx.text.halign.center, pyx.text.size.large]
for (t, title) in enumerate([r"C$_6$", r"C$_7$", r"C$_8$",
                             r"C$_9$", r"C$_{10}$", r"C$_{11}$"]):
    mp.title_box(title, loc = (t+0.5, 4.15), bgcolor=None, text_opts=text_opts)
        
mp.save_fig("multiplot", format="svg")

mp.save_fig("C6_C11.bonds_angles.land", format="svg")

cairosvg.svg2pdf(url='C6_C11.bonds_angles.land.svg',
                 write_to='C6_C11.bonds_angles.land.pdf')
