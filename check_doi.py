#!/usr/bin/env python
#
import os
import sys
import argparse
import textwrap
import bibtexparser
from bibtexparser.bparser import BibTexParser
from bibtexparser.bwriter import BibTexWriter, to_bibtex
from bibtexparser.bibdatabase import BibDatabase
from bibtexparser.customization import *

import textwrap
import requests

from curses import ascii
from pprint import pprint
import json
import string

import requests

punctuation = set(string.punctuation)

def warn(citation_item):
    print("WARNING: Reference with key '{}' not found in the bibliography."
          .format(citation_item.key))

def scrub(value):
    """
    Clean up the incoming queries to remove unfriendly
    characters that may come from copy and pasted citations.

    Also remove all punctuation from text.

    """
    if not value:
        return
    n = ''.join([c for c in value.strip() if not ascii.isctrl(c)])
    #Strip newline or \f characters.
    n2 = n.replace('\n', '').replace('\f', '')
    cleaned = ''.join(ch for ch in n2 if ch not in punctuation)
    return cleaned

def fetch_links(query):
    cr = 'http://search.crossref.org/links'
    #Scrub the citation query.
    cleaned = scrub(query)
    #Turn query into a list because the API is expecting a list.
    citations = [cleaned]
    resp = requests.post(cr, data=json.dumps(citations), headers={'Content-Type': 'application/json'})
    if resp.status_code != 200:
        print "#ERROR citation lookup failed %s." % cleaned
        raise Exception("Crossref request failed")
    match_dict = json.loads(resp.content)
    print "#INFO %s." % match_dict
    results = match_dict.get('results')
    ok = match_dict.get('query_ok')
    found = [r for r in results if r.get('match') is True]
    if len(found) == 0:
        cite_meta = []
    else:
        #Lookup the DOIs and get metadata for the located citations.
        item = found[0]
        doi = item.get('doi')
        meta = fetch_doi(doi)
        cite_meta = [meta]
    out = dict(status=ok, cites=cite_meta)
    return out

def fetch_doi(doi):
    cr = 'http://search.labs.crossref.org/dois'
    #replace / in dois
    resp = requests.get(cr + '?q=' + doi)
    if resp.status_code != 200:
        raise Exception("Crossref request failed.  Error code " + str(resp.status_code))
    meta = json.loads(resp.content)
    #Some doi lookups are failing.  Not sure why.
    #We could also use the CrossRef OpenURL end point to find these:
    #http://www.crossref.org/openurl/?id=doi:10.1016/0166-0934(91)90005-K&noredirect=true&pid=KEY
    try:
        first = meta[0]
        return first
    except IndexError:
        print "#ERROR DOI lookup failed for %s." % doi
        return {}

def doi2bib(doi):
  """
  Return a bibTeX string of metadata for a given DOI.
  """

  url = "http://dx.doi.org/" + doi

  headers = {"accept": "application/x-bibtex"}
  r = requests.get(url, headers = headers)

  return r.text.encode('utf8')

def customizations(record):
    """Use some functions delivered by the library

    :param record: a record
    :returns: -- customized record
    """
    #record = to_latex(record)
    #record = convert_to_unicode(record)
    record = type(record)
    record = author(record)
    record = editor(record)
    record = journal(record)
    record = keyword(record)
    record = link(record)
    record = page_double_hyphen(record)
    record = doi(record)
    return record

if __name__ == '__main__':
	import argparse
	import tempfile
	import citeproc
	from citeproc.source.bibtex import BibTeX

	parser = BibTexParser()
	parser.homogenise_fields = True
	parser.ignore_nonstandard_types = False
	#parser.customization = customizations
	#parser.customization = homogeneize_latex_encoding

	argparser = argparse.ArgumentParser()
	argparser.add_argument("bibfile", help="BibTeX file")
	args = argparser.parse_args()

	with open(args.bibfile, 'r') as bibfile:
		bp = bibtexparser.load(bibfile, parser=parser)
		entries = bp.get_entry_list()
	writer = BibTexWriter()
	bibfiletmp = tempfile.TemporaryFile(mode='w+t')

	# with open('new.bib', 'w') as bibfile:
	# 	bibfile.write(writer.write(bp).encode('UTF-8'))

	try:
		bibfiletmp.write(writer.write(bp).encode('UTF-8'))
		bib_source = citeproc.source.bibtex.BibTeX(args.bibfile)
		bib_style  = citeproc.CitationStylesStyle('harvard1', validate=False)
		bibliography = citeproc.CitationStylesBibliography(bib_style,
															bib_source,
															citeproc.formatter.plain)
		citation_bibtex_map = dict()
		for entry in entries:
			try:
				citation = citeproc.Citation([citeproc.CitationItem(entry['ID'])])
				bibliography.register(citation)
				citation_bibtex_map[entry['ID'].lower()] = entry
			except:
				print '='*70
				print 'Not a bib entry. What is it'
				print entry
				print '='*70
	finally:
		bibfiletmp.close()

	for item in bibliography.items:
		bibcite = ''.join( bib_style.render_bibliography([item])[0] )
		# print item, item.key, bibcite
		print citation_bibtex_map[item.key]['ID']
		ll = fetch_links(bibcite)
		try:
			raw_doi = ll['cites'][0]['doi']
			f_doi = raw_doi.replace('http://dx.doi.org/', '')
			print citation_bibtex_map[item.key]['title']
			print '-'*20+' VERIFICA '+'-'*20
			#print citation_bibtex_map[item.key]['title']
			print ll['cites'][0]['title']
			print '-'*40
			print "Doi                      = {{{0}}},".format(f_doi)
		except:
			print "NAO TEM DOI\n", ll
		print '='*72

	# for item in bibliography.bibliography():
	# 	print(''.join(item))

	# for entry in entries:
	# 	try:
	# 		print doi2bib(entry['doi'])
	# 	except:
	# 		print "No DOI"
	# 		print entry
	# 		# gather string with authors+title+journal
	# 		squery = ' '.join([entry['author'], entry['title']])
	# 		ll = fetch_links(squery)
	# 		raw_doi = ll['cites'][0]['doi']
	# 		f_doi = raw_doi.replace('http://dx.doi.org/', '')
	# 		print('DOI: {0} '.format(f_doi))
	# 		print ll['cites'][0]['fullCitation']
	# 		entry['doi'] = f_doi
	# 		#print entry
	# 	print '='*70
