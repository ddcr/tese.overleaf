%!TEX root = main.tex
\chapter[Liberação de fármaco de matriz esférica]{Liberação de fármaco por difusão de uma matriz polimérica esférica em meio semi-infinito.}
\label{sec:diffusion-spherical}

A difusão de um fármaco, inicialmente disperso numa matriz
polimérica, em contato com um meio externo é um problema muito 
importante em farmacologia.
Este fen\^{o}meno é fundamentalmente caracterizado pela existência de duas 
fases que coexistem durante a difusão do fármaco: na região mais interna da matriz
o soluto encontra-se na fase sólida e disperso, enquanto na região mais externa,
o soluto está dissolvido. A fronteira que separa as duas regiões designa-se por
frente de difusão. O tratamento matemático da transferência de massa por difusão 
é razoavelmente complicado pelo fato de que a frente de difusão ser móvel
(ver a~\autoref{fig:diffusion-spherical}).
\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{drug_release_figs/figs/lee_spherical.pdf}
\caption{\label{fig:diffusion-spherical} Perfil teórico da concentração de 
um fármaco em contato direto com um meio de liberação semi-infinito, isto é, 
um meio em condições de sumidouro perfeito (condições \emph{sink}). Este
diagrama esquemático aplica-se a qualquer sistema matricial onde a liberação
do soluto é unidimensional. A frente de difusão, com posição R, é uma 
fronteira móvel e desloca-se da direita para a esquerda à medida que o 
soluto disperso na região I é exaurido por dissolução. O sistema matricial tem
um tamanho que no caso de uma esfera é igual ao raio \( a \). Para uma matriz
cilíndrica, \( x = a \) seria a distância do eixo. A erosão da matriz é 
ignorada, ou seja, a região II está sempre em contato com o meio externo na 
posição \( x = a \). A concentração \( C_\mathrm{s} \) é a solubilidade do
fármaco na matriz, e A representa a concentração inicial (\emph{drug loading}).
}
\legend{Fonte: adaptado de~\citeonline{Lee:1980a}.}
\end{figure}
À medida que a frente de difusão
avança, deixa atrás de si uma região (região II) onde o soluto encontra-se num
regime transitório (não-estacionário) de difusão. A concentração do soluto nessa
região segue um perfil genérico fixado pelas condições de contorno.
O problema da difusão pode ficar ainda mais complicado se a erosão da matriz for
significativa, porque nesse caso teremos uma segunda fronteira móvel: a
frente de erosão. Problemas deste tipo, onde a transferência de massa dá-se
na presença de uma frente móvel e com mudança de fase, recebem a designação
genérica de ``problemas de Stefan''~\cite[p.~286]{Crank:1975}. Alguns exemplos
pertencentes a esta categoria são os fen\^{o}menos de resfriamento
(acompanhados de solidificação) ou de fusão, de crescimento de cristais, e
de evaporação ou condensação.

A difusão do fármaco num meio isotrópico é governado pela primeira lei de Fick
que relaciona o fluxo, \( \vec{J} \), isto é, a quantidade de matéria 
transferida por unidade de área e por unidade de tempo, com o gradiente
da concentração da espécie:
\begin{equation}
  \vec{J} = -D\vec{\nabla} C
\end{equation} 
onde \( D \) representa o coeficiente de difusão do soluto, assumido 
aqui como independente da concentração \( C \) e, portanto, independente
da posição.
Combinando a primeira lei de Fick com a conservação de massa, podemos
derivar a segunda lei de Fick:
\begin{equation}
 \displaystyle\frac{\partial C}{\partial t} = -\vec{\nabla}\cdot\vec{J}.
\end{equation} 
Em geral a resolução destas equações envolve técnicas de resolução numérica,
mas simplificando alguns aspectos do problema da difusão é possível 
introduzir técnicas semi-analíticas que permitem obter soluções analíticas.
A nossa derivação segue de perto o trabalho de~\citeonline{Lee:1980a}. 
Este autor realizou um trabalho bastante extenso, 
derivando expressões para sistemas matriciais com geometrias esféricas e 
planas, e levando em conta também a erosão. Como nosso propósito
é essencialmente pedagógico, escolhemos a matriz esférica sem erosão.
A difusão com simetria esférica é dada pela seguinte equação diferencial
parcial em coordenadas esféricas:
\begin{equation}\label{eq:fick-spher}
\frac{\partial C}{\partial t} = \frac{1}{r^2}\frac{\partial}{\partial r}
\displaystyle\left(r^2\,D\,\frac{\partial C}{\partial r}\right),
\end{equation}
onde \( r \) é a distância radial.
As condições de contorno para uma matriz não-erodível, não-intumescível (ou
com intumescimento rápido), e cuja difusão do soluto se dá num
meio semi-infinito ideal (sumidouro ideal), são:
\begin{alignat}{2} 
&C[a, t] = 0 \quad             && \mbox{em \( r = a \);}   \label{eq:bcsup}\\
&C[R(t),t] = C_\mathrm{s}\quad && \mbox{em \( r = R(t) \);} \label{eq:bcR}\\
&D\left.\frac{\partial C}{\partial r}\right\vert_{R(t)} = (A-C_\mathrm{s})\frac{dR}{dt} \quad && \mbox{em \( r = R(t). \)} \label{eq:bcRderv}
\end{alignat}
Uma condição de contorno adicional pode ser obtida se combinarmos as Equações~\ref{eq:bcR} e~\ref{eq:bcRderv}. A derivada total 
da~\autoref{eq:bcR} é igual a:
\begin{equation}
\frac{dC}{dt} = 0 \Leftrightarrow
\frac{\partial C}{\partial R}\frac{dR}{dt} + \frac{\partial C}{\partial t} = 0\quad
\mbox{em \(\ r = R(t) \)}.
\end{equation}
Após a substituição dos termos pertinentes, obtemos:
\begin{align}
 {\left(\frac{\partial C}{\partial r}\right)}_{R(t)} \frac{D}{A-C_\mathrm{s}} 
\left(\frac{\partial C}{\partial r}\right) + \frac{1}{r^2}\frac{\partial}{\partial r}
\left(r^2 D \frac{\partial C}{\partial r}\right) = 0 \nonumber \\
{\left(\frac{\partial C}{\partial r}\right)}_{R(t)}^2 + \frac{A-C_\mathrm{s}}{r^2}
\frac{\partial}{\partial r}\left(r^2\frac{\partial C}{\partial r}\right) = 0.
\label{eq:bc-combined}
\end{align}
Continuando, vamos apresentar uma forma mais geral da~\autoref{eq:fick-spher}
introduzindo as seguintes variáveis adimensionais:
\begin{equation}\label{eq:var-adimensional}
r = a(1-\xi);\quad R(t) = a\displaystyle(1-\delta(t))\quad\mbox{e}\quad t\frac{D}{a^2} = \tau.
\end{equation}
A concentração \( C \) também pode ser substituída pela seguinte 
função adimensional:
\begin{equation}\label{eq:c-adimensional}
\theta = \frac{r}{a}
\biggl(\frac{C_\mathrm{s}-C}{C_\mathrm{s}}\biggr) 
\Leftrightarrow
\frac{C}{C_\mathrm{s}} = 1-\frac{\theta(\xi)}{1-\xi}.
\end{equation}
A segunda lei da difusão pode ser então reescrita da seguinte maneira:
\begin{equation}\label{eq:fick-versao-1}
\frac{\partial\hfill}{\partial \tau}\left(\frac{C}{C_\mathrm{s}}\right) = 
\frac{1}{{(1-\xi)}^2}\frac{\partial}{\partial\xi}\left[{(1-\xi)}^2\frac{\partial}{\partial\xi}\left(\frac{C}{C_\mathrm{s}}\right)\right].
\end{equation}
O termo da derivada do lado direito da~\autoref{eq:fick-versao-1} 
(entre parênteses) pode ser determinado a partir da derivada de 
\( \theta \) (\autoref{eq:c-adimensional}) em função de \( \xi \):
\begin{align}
\frac{\partial}{\partial\xi}\left(\frac{C}{C_\mathrm{s}}\right) = 
-\frac{1}{1-\xi}\left(\frac{\partial\theta}{\partial\xi}\right)-
\frac{\theta}{{(1-\xi)}^2},\,\mbox{ isto é,} \nonumber \\
{(1-\xi)}^2 \frac{\partial}{\partial\xi}\left(\frac{C}{C_\mathrm{s}}\right) = 
-(1-\xi)\frac{\partial\theta}{\partial\xi}-\theta. \label{eq:ficks-rhs}
\end{align}
Derivando novamente a expressão~\ref{eq:ficks-rhs} em função de \( \xi \),
mais a derivada de \( \theta \)~\eqref{eq:c-adimensional} em função 
de \( \tau \), chegamos às seguintes expressões:
\begin{align}
\frac{\partial}{\partial\xi} \left[{(1-\xi)}^2\frac{\partial}{\partial\xi}\left(\frac{C}{C_\mathrm{s}}\right)\right] = -(1-\xi)\frac{\partial^2\theta}{\partial\xi^2} \nonumber \\
\frac{\partial}{\partial\tau}\left(\frac{C}{C_\mathrm{s}}\right) = 
-\frac{1}{1-\xi}\frac{\partial\theta}{\partial\tau}.
\end{align}
A combinação destas expressões leva-nos à segunda lei de Fick
segundo as coordenadas adimensionais:
\begin{equation}\label{eq:fick-adimensional}
\frac{\partial\theta}{\partial\tau} = 
\frac{\partial}{\partial\xi}\left(\frac{\partial\theta}{\partial\xi}\right).
\end{equation}

As condições de contorno,~\ref{eq:bcsup},~\ref{eq:bcR} e~\ref{eq:bcRderv} 
também podem ser reescritas usando as variáveis adimensionais:
\begin{alignat}{2} 
&\theta[0, \tau] = 1\quad             && \mbox{em \( \xi = 0 \);} 
\label{eq:bcsup-ad} \\
&\theta[\delta(\tau),\tau] = 0\quad && \mbox{em \( \xi = \delta(\tau) \);} 
\label{eq:bcR-ad} \\
&\left.\frac{\partial\theta}{\partial\xi}\right\vert_{\delta} = 
(1-\delta)
\biggl(1-\frac{A}{C_\mathrm{s}}\biggr)
\frac{d\delta}{d\tau}\quad && \mbox{em \( \xi = \delta(\tau) \)}.
\label{eq:bcRderv-ad}
\end{alignat}
A condição de contorno~\eqref{eq:bc-combined} é por sua vez igual a:
\begin{equation}\label{eq:bc-combined-ad}
-{\left(\frac{\partial\theta}{\partial\xi}\right)}_{\xi=\delta}^2 = 
\biggl(1-\frac{A}{C_\mathrm{s}}\biggr)
(1-\delta)
{\left(\frac{\partial^2\theta}{\partial\xi^2}\right)}_{\xi=\delta},
\quad \mbox{em \( \xi = \delta(\tau) \)}.
\end{equation}

Uma vez estabelecidas estas expressões em coordenadas adimensionais, 
falta resolver a equação diferencial parcial~\eqref{eq:fick-adimensional} 
para obter a posição da frente de difusão, \( \delta \), em função 
do tempo, \( \tau \). Este é um resultado intermediário que será 
necessário, como iremos mostrar adiante, para determinar a quantidade
de fármaco liberada pela matriz por unidade de área no instante
de tempo \( t \). Em vez de resolvermos numericamente de maneira direta 
a~\autoref{eq:fick-adimensional}, vamos utilizar uma aproximação, designada
por método do balanço integral, uma técnica de resolução introduzida 
por~\apudonline[Cap.~13.6.2, p.~312]{Goodman:1964}{Crank:1975}.
A ideia subjacente ao método consiste em postular 
uma forma funcional para o perfil da concentração do fármaco
e resolver a~\autoref{eq:fick-adimensional} por integração
direta (mais detalhes sobre o método podem ser também consultados 
no artigo de revisão por~\cite{Mitchell:2010}). 
Vamos então integrar a~\eqref{eq:fick-adimensional} 
na região II da~\autoref{fig:diffusion-spherical}:
\begin{align}
\displaystyle\int_{\delta}^{\xi}
\left(\frac{\partial\theta}{\partial\tau}\right)\,\mathrm{d}\xi =
\left(\frac{\partial\theta}{\partial\xi}\right) - 
{\left(\frac{\partial\theta}{\partial\xi}\right)}_{\xi=\delta},
\quad \mbox{(primeira integração)} \nonumber \\
\displaystyle\int_{\delta}^{0}\,\mathrm{d}\xi\displaystyle\int_{\delta}^{\xi}
\left(\frac{\partial\theta}{\partial\tau}\right)\,\mathrm{d}\xi = 
\int_{\delta}^{0}\left[
\left(\frac{\partial\theta}{\partial\xi}\right) -
{\left(\frac{\partial\theta}{\partial\xi}\right)}_{\delta}
\right]\,\mathrm{d}\xi,\quad \mbox{(segunda integração)}. \label{eq:duplaint}
\end{align}
Após a dupla integração podemos usar a expressão~\ref{eq:bcRderv-ad} para 
substituir o segundo termo do integral do lado direito da~\autoref{eq:duplaint}.
Com isto obtemos a seguinte expressão:
\begin{equation}
\displaystyle\int_{\delta}^{0}\,\mathrm{d}\xi\displaystyle\int_{\delta}^{\xi}
\left(\frac{\partial\theta}{\partial\tau}\right)\,\mathrm{d}\xi = 
\int_{\delta}^{0}\left(\frac{\partial\theta}{\partial\xi}\right) -
\biggl(1-\frac{A}{C_\mathrm{s}}\biggr)\,(1-\delta)\,\delta \frac{d\delta}{d\tau}
\end{equation}
Podemos desenvolver mais os termos dentro dos integrais: 
\begin{align}
\displaystyle\int_{\delta}^{0}\,\mathrm{d}\xi
\frac{d}{d\tau}\biggl(\displaystyle\int_{\delta}^{\xi}
\theta[\xi,\tau]\,\mathrm{d}\xi \biggr) = 
\int_{\delta}^{0}\left(\frac{\partial\theta}{\partial\xi}\right) -
\biggl(1-\frac{A}{C_\mathrm{s}}\biggr)\,
\frac{d}{d\tau}\biggl(\frac{\delta^2}{2}-\frac{\delta^3}{3}\biggr),\quad\mbox{isto é}
\nonumber \\
\frac{d}{d\tau}\biggl[
\displaystyle\int_{\delta}^0\,\mathrm{d}\xi \int_{\delta}^{\xi}\theta \mathrm{d}\xi
+\biggl(1-\frac{A}{C_\mathrm{s}}\biggr)\biggl(\frac{\delta^2}{2}-\frac{\delta^3}{3}\biggr)
\biggr] = \int_{\delta}^{0}\left(\frac{\partial\theta}{\partial\xi}\right) =
\underbrace{\theta[\xi=0]}_{ = 1} - \underbrace{\theta[\xi=\delta]}_{ = 0}.
\label{eq:tmp1}
\end{align}

Seguindo~\citeonline{Lee:1980a} vamos escalonar a coordenada espacial \( \xi \) 
em função da posição da frente de difusão, \( \delta \), \( \eta = \xi / \delta \),
para simplificar ainda mais a aparência da expressão~\eqref{eq:tmp1}:
\begin{align}
& \frac{d}{d\tau}\biggl[\delta^2 g(\eta)\biggr] = 1,\quad\mbox{onde,} \\
& g(\eta) = \int_1^0\,\mathrm{d}\eta\int_1^{\eta}\theta \mathrm{d}\eta-
\biggl(1-\frac{A}{C_\mathrm{s}}\biggr)\biggl(\frac{1}{2}-\frac{\delta}{3}\biggr).
\label{eq:g}
\end{align}
Vamos agora aproximar a concentração do fármaco na região de regime 
transitório (região II) pelo seguinte polin\^{o}mio:
\begin{equation}\label{eq:postulate}
\theta[\eta] = a_1 + a_2 \eta + a_3 \eta^2.
\end{equation}
Os coeficientes \( a_1, a_2 \) e \( a_3 \) podem ser determinados a partir 
das condições de contorno do problema. 
As Equações~\ref{eq:bcsup-ad} e~\ref{eq:bcR-ad} restringem
os valores de \( a_1 \) e \( a_2 \) a:
\begin{align}
& a_1 = 1,\quad \mbox{e} \\
& a_2 = -(1 + a_3).
\end{align}
Por sua vez a condição de contorno~\ref{eq:bc-combined-ad} dá origem ao termo 
\( a_3 \):
\begin{equation}
a_3 = 1 - \biggl(1-\frac{A}{C_\mathrm{s}}\biggr)(1-\delta) -
{\Biggl(
{\biggl[ 1-\biggl(1-\frac{A}{C_\mathrm{s}}\biggr)(1-\delta) \biggr]}^2 - 1
\Biggr)}^{\frac{1}{2}}.
\end{equation}
Substituindo a expressão~\eqref{eq:postulate} em~\eqref{eq:g} \( g \) 
resulta:
\begin{align}
g[\delta] = \frac{1}{12}\biggl[6\left(
\frac{A}{C_\mathrm{s}}\right) -4 -a_3 \biggr] -
\frac{1}{3}\biggl(\frac{A}{C_\mathrm{s}}-1\biggr)\delta.
\end{align}
A posição da frente de difusão, \( \delta(\tau) \), é determinada como uma
função implícita do tempo, \( \tau \),
\begin{equation}
\tau = \delta(\tau) g[\delta(\tau)].
\end{equation}

Estamos agora em condições de determinar a quantidade acumulada de massa de 
fármaco que foi transferida através da frente de difusão, no final do 
tempo \( t \), considerando o seguinte balanço de massa:
\begin{equation}
M(t) = \frac{4}{3}\pi A \displaystyle\left(a^3-{R(t)}^3\right) - \int_{R(t)}^a 4\pi r^2 C[r]\,\mathrm{d}r.
\end{equation}
Do ponto de vista experimental tem mais utilidade a fração de massa liberada, 
\( M(t)/M_\infty \), onde \( M_\infty \) representa a massa total do soluto
na matriz, e que teoricamente seria liberada totalmente em tempo infinito.
Como \( M_\infty = 4/3\pi A a^3 \), a fração da massa liberada no tempo t é 
igual a:
\begin{flalign}
\frac{M}{M_\infty} &= 1 -{\left(\frac{R}{a}\right)}^3 - 
3\left(\frac{C_\mathrm{s}}{A}\right)
\displaystyle\int_{\delta(\tau)}^0\,\mathrm{d}\xi {(1-\xi)}^2\,
\biggl[1-\frac{\theta[\xi,\tau]}{1-\xi}\biggr]
\nonumber \\
\frac{M}{M_\infty} &= 1-{(1-\delta)}^3 - 3\left(\frac{C_\mathrm{s}}{A}\right)\delta
\int_1^0\,\mathrm{d}\eta {(1-\delta\eta)}^2\,
\biggl[1-\frac{a_1+a_2\eta+a_3\eta^3}{1-\delta\eta}\biggr].
\end{flalign}
Após este exercício de álgebra, chegamos a uma expressão relativamente simples
para a fração de massa de soluto liberada em função da posição da frente de
difusão, e implicitamente, do tempo \( t \):
\begin{multline}
\frac{M}{M_\infty} = \left[1-{(1-\delta)}^3\right]\biggl(1-\frac{C_\mathrm{s}}{A}\biggr) 
+ 3\left(\frac{C_\mathrm{s}}{A}\right)\delta
\biggl[\biggl(a_1 + \frac{a_2}{2} + \frac{a_3}{3}\biggr)- \\
\biggl(\frac{a_1}{2}+\frac{a_2}{3}+\frac{a_3}{4}\biggr)\delta\biggr].
\end{multline}


\chapter[Liberação de fármaco de matriz cilíndrica]{Liberação de fármaco por difusão de uma matriz polimérica cilíndrica em meio semi-infinito.}
\label{sec:diffusion-cylinder}

A formulação deste problema é em tudo igual ao caso anterior com a exceção 
de que agora se trata de uma matriz polimérica com geometria cilíndrica 
(e.g., caso de um comprimido) e com a ressalva de que a contribuição da 
difusão pelas bases do cilindro será desprezada no cálculo da massa 
total de fármaco liberada.
As variáveis da matriz são a altura, \( H \), seu raio inicial, \( S \) e 
a posição da sua frente de difusão,\( R(t) \) 
(ver~\autoref{fig:diffusion-cylinder}). Neste caso, também é assumido,
para simplificação do problema, que o coeficiente de difusão, \( D \), 
independe da posição e da concentração do soluto na matriz.
\begin{figure}[htb]
\centering
\includegraphics[width=0.6\textwidth]{drug_release_figs/figs/cylinder_tablet.pdf}
\caption{\label{fig:diffusion-cylinder}Diagrama esquemático de uma matriz 
cilíndrica com o soluto parcialmente liberado: \( R(t) \) é a distância, 
relativa ao eixo da matriz, da frente de difusão e \( S \) representa a
distância da superfície em contato com o meio externo.}
\legend{Fonte: adaptado de~\citeonline{Khamene:2015}.}
\end{figure}
Como o processo de difusão é assumido unidimensional e ao longo da distância 
radial, \( \rho \), o perfil da concentração do fármaco segue exatamente o 
perfil esquemático da matriz esférica, tal como é mostrado
 na~\autoref{fig:diffusion-spherical}.

A segunda lei de Fick em sistemas matriciais com simetria cilíndrica é dada
pela seguinte equação diferencial parcial:
\begin{equation}\label{eq:pde-cyl}
\frac{\partial C}{\partial t} = \frac{1}{\rho}\frac{\partial}{\partial \rho}
\displaystyle\left(\rho D\,\frac{\partial C}{\partial \rho}\right),
\end{equation}
onde a difusão axial foi desprezada. As condições de contorno, de modo semelhante
ao caso esférico, são iguais a:
\begin{alignat}{2} 
&C[S, t] = 0 \quad             && \mbox{em \( \rho = S \);}   \label{eq:bcsup-cyl}\\
&C[R(t),t] = C_\mathrm{s}\quad && \mbox{em \( \rho = R(t) \);} \label{eq:bcR-cyl}\\
&D\left.\frac{\partial C}{\partial \rho}\right\vert_{R(t)} = (A-C_\mathrm{s})\frac{dR}{dt} \quad && \mbox{em \( \rho = R(t). \)} \label{eq:bcRderv-cyl}
\end{alignat}
Para reduzir a equação diferencial parcial~\eqref{eq:pde-cyl} à sua 
forma adimensional, introduzimos o seguinte escalonamento das variáveis:
\begin{equation}\label{eq:var-adimensional-cyl}
\xi = \frac{\rho}{S};\quad \delta = \frac{R(t)}{S};\quad t\frac{D}{S^2} = \tau
\quad \mbox{e}\quad \theta = \frac{C}{C_\mathrm{s}}.
\end{equation}
A forma reduzida da segunda lei de Fick para este sistema fica então igual a:
\begin{equation}\label{eq:fick-adimensional-cyl}
\frac{\partial\theta}{\partial\tau} = \frac{1}{\xi}
\frac{\partial}{\partial\xi}\left(\xi\frac{\partial\theta}{\partial\xi}\right).
\end{equation}
As condições de contorno na sua forma adimensional escrevem-se da seguinte 
maneira:
\begin{alignat}{2} 
&\theta[\delta(\tau),\tau] = 1\quad && \mbox{em \( \xi = \delta(\tau) \);} 
\label{eq:bcR-cyl-ad} \\
&\left.\frac{\partial\theta}{\partial\xi}\right\vert_{\delta} = 
\biggl(\frac{A}{C_\mathrm{s}}-1\biggr)
\frac{d\delta}{d\tau}\quad && \mbox{em \( \xi = \delta(\tau) \)}. \label{eq:bcRderv-cyl-ad}
\end{alignat}

Para resolver a~\autoref{eq:fick-adimensional-cyl} podemos aplicar a chamada
técnica de similaridade que reduz a equação diferencial parcial numa simples
equação diferencial ordinária mediante a seguinte combinação das variáveis 
\( \xi \) e \( \tau \)~\cite{Khamene:2015}
\begin{equation}
U^2 = \frac{\xi^2}{4\tau}.
\end{equation}
A justificativa deste procedimento resulta da invariância
 da~\autoref{eq:fick-adimensional-cyl} a famílias paramétricas
de soluções do tipo
\begin{align}
&\tilde{\xi}  = {\lambda}^a \xi \nonumber \\
&\tilde{\tau} = {\lambda}^b \tau \nonumber \\
&\tilde{\theta}(\tilde{\xi}, \tilde{\tau}) = {\lambda}^c 
\theta(\lambda^{-b}\tilde{\xi}, \lambda^{-a}\tilde{\tau}),
\end{align}
onde \( 2a -b = 0 \). Como
\begin{align}
\tilde{\theta}\,\displaystyle{\tilde{\tau}}^{-\frac{c}{b}} = 
\theta \displaystyle{\tau}^{-\frac{c}{b}} \\
\frac{\tilde{\xi}^2}{\tilde{\tau}} = \frac{\xi^2}{\tau},
\end{align} 
isto sugere-nos que procuremos soluções do tipo \( \theta = \tau^{b/c} y(U) \)
com \( U \sim \xi/\sqrt{\tau}\)~\cite{Dresner:1988}.

Como transformar a~\autoref{eq:fick-adimensional-cyl} numa equação
diferencial ordinária? Sabemos que:
\begin{align}
& \frac{\partial\theta}{\partial\tau} = 
\frac{d\theta}{dU}\frac{\partial U}{\partial\tau} \nonumber \\
& \frac{\partial\theta}{\partial\xi}  = 
\frac{d\theta}{dU}\frac{\partial U}{\partial\xi}\quad\mbox{e} \nonumber \\
& \frac{\partial}{\partial\xi}\frac{\partial\theta}{\partial\xi} = 
\frac{d}{dU}\left(\frac{\partial\theta}{\partial\xi}\right)\frac{\partial U}{\partial\xi} 
= \frac{d^2\theta}{dU^2}{\left(\frac{\partial U}{\partial\xi}\right)}^2,
\end{align}
logo a equação de difusão transforma-se na seguinte equação diferencial:
\begin{equation}
\frac{d^2\theta}{dU^2} = -\biggl(2U+\frac{1}{U}\biggr)\,\frac{d\theta}{dU},
\end{equation}
cuja solução é igual a
\begin{equation}\label{eq:sol}
\theta[\xi, \tau] = \theta[U] = C_1 + C_2\,\operatorname{Ei}(-U^2),
\end{equation}
onde \( C_1 \) e \( C_2 \) são constantes de integração. A função
\( \operatorname{Ei}(x) \) é designada por função exponencial 
integral e é definida por
\begin{equation}
\operatorname{Ei}(x) = \displaystyle\int_{-\infty}^x\,
\frac{\mathrm{e}^t}{t}\,\mathrm{d}t,
\end{equation}
no domínio dos números reais. Se aplicarmos as condições de contorno
à~\autoref{eq:sol} podemos determinar \( C_1 \) e \( C_2 \) e, 
consequentemente, a concentração \( \theta \):
\begin{equation}
\theta = \frac{\operatorname{Ei}\left(-\displaystyle\frac{\xi^2}{4\tau}\right) - 
\operatorname{Ei}\left(-\displaystyle\frac{1}{4\tau}\right)}
{\operatorname{Ei}\left(-\displaystyle\frac{\delta^2}{4\tau}\right) -
\operatorname{Ei}\left(-\displaystyle\frac{1}{4\tau}\right)}.
\end{equation}
Temos então a concentração \( \theta \) em função da posição da frente de 
difusão, \( \delta \), e da distância \( \xi \), mas precisamos
determinar explicitamente \( \delta \) em função do tempo, e para isso 
recorremos à~\autoref{eq:bcRderv-cyl-ad}. Daqui resulta a seguinte
equação diferencial:
\begin{equation}
\displaystyle\frac{2\,\mathrm{e}^{-\displaystyle\frac{\delta^2}{4\tau}}}
{\delta \,\displaystyle\left(\operatorname{Ei}\left(-\frac{\delta^2}{4\tau}\right) - 
\operatorname{Ei}\left(-\frac{1}{4\tau}\right)\right)} = 
\biggl(\frac{A}{C_\mathrm{s}}-1\biggr)\frac{d\delta}{d\tau},
\end{equation}
que só pode ser resolvida numericamente.

A quantidade acumulada de soluto liberada no tempo \( t \), \( M(t) \), é dado
pelo seguinte balanço de massa
\begin{align}
&M(t) = \pi HA\left(S^2-{R(t)}^2\right) - 
\int_{R}^S 2\pi\rho H C[\rho,t]\,\mathrm{d}\rho,\quad\mbox{com} \\
&M_\infty = \pi HA S^2 \nonumber, 
\end{align}
ou seja,
\begin{equation}
\frac{M(t)}{M_\infty} = 1 - \biggl(1-\frac{C_\mathrm{s}}{A}\biggr)\delta^2 +
\biggl(\frac{C_\mathrm{s}}{A}\biggr)
\displaystyle\Bigg[
\frac{\mathrm{e}^{-\displaystyle\frac{\delta^2}{4\tau}} 
- \mathrm{e}^{-\displaystyle\frac{1}{4\tau}}}
{\operatorname{Ei}\left(-\frac{\delta^2}{4\tau}\right)
-\operatorname{Ei}\left(-\frac{1}{4\tau}\right)}
\Bigg]\,\tau.
\end{equation}


 \chapter[Liberação de fármaco de matriz plana]{Liberação de fármaco por difusão de uma matriz polimérica plana em meio semi-infinito: modelo de Higuchi.}
\label{sec:diffusion-planar}

A liberação do fármaco de um matriz plana (membrana) é um problema idêntico
ao modelado originalmente por~\citeonline{Higuchi:1961}. A formulação deste
problema é em tudo semelhante aquele descrito para o caso esférico 
no~\autoref{sec:diffusion-spherical}. 
Tal como nesse caso, a difusão aqui também é unidimensional e 
a~\autoref{fig:diffusion-spherical} aplica-se de igual modo neste
contexto, com a diferença que o raio \( a \) é substituído pela espessura
da matriz, \( H \) (ver~\autoref{fig:diffusion-planar}). 
\begin{figure}[htb]
\centering
\includegraphics[width=0.8\textwidth]{drug_release_figs/figs/higuchi_planar.pdf}
\caption{\label{fig:diffusion-planar}Diagrama esquemático de um sistema
matricial plano semi-infinito (membrana ou filme que se estende ao infinito
 na direção y) com espessura \( H \), em contato com um meio 
externo (\( x > H \)) no regime de sumidouro ideal.
O soluto encontra-se parcialmente liberado na região II:\@ \( R(t) \) 
é a distância da frente de difusão.}
\legend{Fonte: {\legendauthor}.}
\end{figure}
A equação de difusão~\eqref{eq:fick-spher} para o caso 
da membrana, ou filme, é dada pela seguinte equação diferencial:
\begin{equation}\label{eq:fick-planar}
\frac{\partial C}{\partial t} = \frac{\partial}{\partial x}
\displaystyle\left(D\,\frac{\partial C}{\partial x}\right),
\end{equation}
onde \( x \) é a coordenada na direção da difusão, isto é, ao longo da espessura da 
membrana.
O modelo original de Higuchi assume que o regime de difusão na região II é um
regime quase-estacionário. A concentração \( C(x,t) \) é linear nesse caso, o que
justifica-se enquanto \( A \gg C_\mathrm{s} \):
\begin{equation}\label{eq:pseudoc}
C[x,t] = C_\mathrm{s} - \frac{C_\mathrm{s}}{H-R(t)}(x-R(t)).
\end{equation}
A forma funcional da equação acima deriva das condições de 
contorno \( C[x=H, t] = 0 \) e \( C[x=R(t), t] = C_\mathrm{s} \).
A quantidade de massa acumulada de soluto que é liberada no instante de 
tempo \( t \) é dada pelo seguinte balanço de massa:
\begin{align}
M(t) &= \left[H - R(t)\right]\,A - \int_{R(t)}^H C[x, t]\,\mathrm{d}x \nonumber \\
     &= \left[H - R(t)\right]\,A - \frac{1}{2} C_\mathrm{s}\left[H - R(t)\right] \nonumber \\
     &= \biggl(A-\frac{C_\mathrm{s}}{2}\biggr)\left[H - R(t)\right].
\end{align}
De acordo com~\citeonline{Higuchi:1961} a outra condição de contorno na frente de 
difusão, \( R(t) \), é igual a:
\begin{equation}\label{eq:bc-strange-higuchi}
D\left.\frac{\partial C}{\partial x}\right\vert_{R(t)} = 
\biggl(A-\frac{C_\mathrm{s}}{2}\biggr)\frac{dR}{dt}.
\end{equation}  
Integrando a~\autoref{eq:bc-strange-higuchi} com  a ajuda de~\eqref{eq:pseudoc} obtemos
a expressão para a frente de difusão:
\begin{equation}
\frac{4DC_\mathrm{s}}{2A-C_\mathrm{s}}t = {(R-H)}^2.
\end{equation}
Daqui tiramos facilmente que a massa liberada de fármaco no tempo \( t \) é
dada pela seguinte expressão:
\begin{equation}
M(t) = \sqrt{(2A-C_\mathrm{s})D\,C_\mathrm{s}\,t}.
\end{equation}
