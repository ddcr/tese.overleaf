#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
#
#
# Copyright (C) 2015 Domingos Rodrigues <ddcr@lcc.ufmg.br>
#
# Created: Wed Sep 16 21:31:51 2015 (BRT)
#
# $Id$
#

__id__ = "$Id:$"
__revision__ = "$Revision:$"

import math, sys
import pandas as pd
from collections import OrderedDict
from bokeh.io import output_notebook
from bokeh.plotting import figure, gridplot, vplot, hplot, output_file, show
from bokeh.models import (ColumnDataSource, HoverTool, FixedTicker, Circle,
                          Square, Callback, CustomJS)
from bokeh.models.widgets import DataTable, Paragraph, TableColumn, NumberFormatter
from bokeh.models.axes import LinearAxis


TITLE="Genetic algorithm optimization of matrix physicochemical properties in drug release studies"

output_file("drug_release_interactive.html", title=TITLE)
output_notebook()


def mscatter(x, y, source, x_axis_label=None, y_axis_label=None, reference=None):
    """Various scatter plots
    """
    
    if x_axis_label is None:
        x_axis_label = x
    if y_axis_label is None:
        y_axis_label = y

    f = figure(title=None,
               x_axis_label = x_axis_label,
               y_axis_label = y_axis_label,
               plot_width = P['plotW'],
               plot_height= P['plotH'],
               tools      = P['tools'],
               )

    f.xaxis.axis_label_text_font_size = "10pt"
    f.yaxis.axis_label_text_font_size = "10pt"
    f.xaxis.axis_label_text_color = "blue"
    f.yaxis.axis_label_text_color = "blue"
    
    f.border_fill='whitesmoke'
    
    fs = f.scatter(x, y,
                   source = source,
                   alpha  = P['alpha'],
                   color  = P['def_color'],
                   size   = P['size'],
                   name   = "f_handle")

    # configure hovertool
    hov = f.select(dict(type=HoverTool))
    hov.point_policy = "snap_to_data"
    hov.tooltips = OrderedDict([("Point", "$index"),
                                (x_axis_label, "@"+x),
                                (y_axis_label, "@"+y)])

    if reference is not None:
        f.title_text_font_size = "10pt"
        f.title_text_color = "blue"

        # add point, then add the crosshair
        fs_ref = f.scatter(reference[x].values, reference[y].values,
                           alpha  = P['alpha'],
                           color  = 'blue',
                           size   = P['size'],
                           marker = 'o+',
                           name   = "f_handle_ref")

        # hackish way of annotating the endpoints of the crosshair
        xref = LinearAxis(ticker=FixedTicker(ticks=reference[x].values))
        yref = LinearAxis(ticker=FixedTicker(ticks=reference[y].values))
        f.add_layout(xref, 'above')
        f.add_layout(yref, 'right')
        
        # crosshair: draw horizontal ray
        f.ray(x = reference[x].values, y = reference[y].values,
              length=0, angle = 0.0, line_width=2, line_dash='2 2')
        f.ray(x = reference[x].values, y = reference[y].values,
              length=0, angle = math.pi, line_width=2, line_dash='2 2')
        
        # crosshair: draw vertical ray
        f.ray(x = reference[x].values, y = reference[y].values,
              length=0, angle = 0.5*math.pi, line_width=2, line_dash='2 2')
        f.ray(x = reference[x].values, y = reference[y].values,
              length=0, angle = -0.5*math.pi, line_width=2, line_dash='2 2')

    renderer = f.select(name="f_handle")[0]
    renderer.nonselection_glyph = Circle(line_width = 1,
                                         line_alpha = 0.2,
                                         fill_color = "white",
                                         line_color = "gray")
    renderer.selection_glyph    = Circle(fill_alpha = 1,
                                         fill_color = "firebrick",
                                         line_color = None)
    return f

# reference solution
params_ref = pd.DataFrame({'Dm5': [1.35],
                           'Cs': [16.2],
                           'A': [70.0],
                           'h': [0.167]})

# read output of genetic algorithm
params_ag = pd.read_csv('Solucoes.csv', header=0)
df        = params_ag[['Dm5','Cs','A','h']]

# ELIMINATE FALSE SOLUTIONS: only consider data with R=A/Cs > 1, the domain
#                            of the Higuchi model
df_valid = df[df['A']/df['Cs'] > 1.0]

# prepare data to BOKEH
data = ColumnDataSource(df_valid)


#######################
#
#        GRAPHICS
#
#######################

avail_tools = 'pan, box_zoom, box_select, lasso_select, reset, save, hover',
P = {
    'tools'     : avail_tools,
    'def_color' : 'orangered',
    'fontsize'  : '9',
    'alpha'     : 0.8,
    'size'      : 5,
    'canvasW'   : 1000,
    }

# optimal aspect ratio
golden_mean = 0.5*(math.sqrt(5.0)-1.0)
P['canvasH'] = int(golden_mean*P['canvasW'])

Nx = 3
Ny = 2
P['plotW'], P['plotH'] = int(P['canvasW']/Nx), int(P['canvasH']/Ny)

Dm5_fmt = 'D/1e-5 (cm^2/day)'
A_fmt   = 'A (mg/cm^3)'
Cs_fmt  = 'Cs (mg/cm^3)'
h_fmt   = 'h (cm)'

f1 = mscatter('Dm5', 'A',
              data,
              x_axis_label = Dm5_fmt,
              y_axis_label = A_fmt,
              reference    = params_ref)
f2 = mscatter('Dm5', 'Cs',
              data,
              x_axis_label = Dm5_fmt,
              y_axis_label = Cs_fmt,
              reference    = params_ref)
f3 = mscatter('Dm5', 'h',
              data,
              x_axis_label = Dm5_fmt,
              y_axis_label = h_fmt,
              reference    = params_ref)
f4 = mscatter('A',   'Cs',
              data,
              x_axis_label = A_fmt,
              y_axis_label = Cs_fmt,
              reference    = params_ref)
f5 = mscatter('A',   'h',
              data,
              x_axis_label = A_fmt,
              y_axis_label = h_fmt,
              reference    = params_ref)
f6 = mscatter('Cs',  'h',
              data,
              x_axis_label = Cs_fmt,
              y_axis_label = h_fmt,
              reference    = params_ref)
    
p = gridplot([[f1, f2, f3], [f4, f5, f6]],
             toolbar_location = "above",
             title            = TITLE)

#######################
#
#        TABLE
#
#######################

columns = [
    TableColumn(field="Dm5", title=Dm5_fmt, formatter=NumberFormatter(format="0.0000")),
    TableColumn(field="A",   title=A_fmt,   formatter=NumberFormatter(format="0.0000")),
    TableColumn(field="Cs",  title=Cs_fmt,  formatter=NumberFormatter(format="0.0000")),
    TableColumn(field="h",   title=h_fmt,   formatter=NumberFormatter(format="0.0000")),
]

data_table = DataTable(source     = data,
                       columns    = columns,
                       width      = 500,
                       editable   = True,
                       selectable = 'checkbox')


data.callback = CustomJS(args=dict(data=data,data_table=data_table), code="""
           var inds = cb_obj.get('selected')['1d'].indices;
           var d = cb_obj.get('data');
           """)


layout = vplot(Paragraph(text=TITLE),
               p,
               Paragraph(text=""), # space hack
               data_table)
show(layout)
