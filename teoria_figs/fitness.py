#!/usr/bin/env python
"""Summary
"""
#
import numpy as np
# import matplotlib as mpl
import matplotlib.pyplot as plt


def energy_sigmoid(rho):
    """fitness function

    Args:
        rho (float): normalised value of the energy

    Returns:
        float: fitness function

    """
    return 0.5*(1.0-np.tanh(2.0*rho-1.0))


fig = plt.figure(1)
ax0 = fig.add_subplot(111)
plt.subplots_adjust(bottom=0.15)

rho = np.linspace(0.0, 1.0, 100)
plt.plot(rho,
         energy_sigmoid(rho),
         color='k',
         ls='-',
         lw=2,
         marker='None')

# draw vertical + horizontal dotted lines
xvert = np.linspace(0.0, 1.0, 20)
yvert = energy_sigmoid(xvert)
for x1, y1 in zip(xvert, yvert):
    plt.plot((x1, x1), (0.0, y1), 'k:')
    plt.plot((0.0, x1), (y1, y1), 'k:')

plt.xlabel(r'$\rho$'+'\n'+r'$\longleftarrow \mathrm{V}_\min$',
           multialignment='center')
plt.ylabel(r'F($\rho$) [fitness]')
plt.axis([0.0, 1.0, 0.0, 1.0])
# plt.show()
plt.savefig('tanh_fitness.pdf', dpi=300)