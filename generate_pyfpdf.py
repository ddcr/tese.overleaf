#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: ddcr
# @Date:   2016-08-03 16:40:22
# @Last Modified by:   ddcr
# @Last Modified time: 2016-08-03 21:20:54
from fpdf import FPDF


def lt(str):
    return unicode(str, 'utf-8').encode('iso-8859-1')


def generate_pyfpdf(loremipsum, file='ficha_catalografica.pdf'):
    """Gera a folhas que deverão ser substituídas pela
    biblioteca.

    Args:
        loremipsum (string): Texto
        pdffile (str, optional): Arquivo PDF

    Returns:
        None:
    """

    pdf = FPDF('P', unit='mm', format='A4')
    # set margins BEFORE add_page!
    pdf.l_margin = pdf.l_margin*4.0
    pdf.r_margin = pdf.r_margin*4.0
    pdf.t_margin = pdf.t_margin*4.0
    pdf.b_margin = pdf.b_margin*4.0
    pdf.add_page()

    # Effective page width and height
    epw = pdf.w-(pdf.l_margin+pdf.r_margin)
    eph = pdf.h-(pdf.t_margin+pdf.b_margin)
    # Draw new margins
    pdf.rect(pdf.l_margin, pdf.t_margin, w=epw, h=eph)

    pdf.set_font('Times', '', 18)
    # Text height
    th = pdf.font_size
    pdf.ln(10*th)

    pdf.multi_cell(epw, 10,
                   lt(loremipsum), align='C')
    # writes pdf and closes
    pdf.output(file, 'F')


if __name__ == '__main__':
    loremipsum = """Ficha catalográfica\n
    fornecida pela\n
    biblioteca.
    """
    generate_pyfpdf(loremipsum)

    loremipsum = """Folha de Aprovação"""
    generate_pyfpdf(loremipsum, file='folhadeaprovacao_final.pdf')
