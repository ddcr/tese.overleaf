%!TEX root = main.tex
\chapter[Conclusões e perspectivas.]{Conclusões e perspectivas.}
\label{sec:conclusoes}

A previsão das estruturas at\^{o}micas de materiais é de suma importância.
Isto se aplica com mais propriedade no caso de nanoclusters já que
estes sistemas exibem propriedades químicas e físicas 
substancialmente diferentes quando comparados com o \emph{bulk}.
Uma compreensão plena das transformações estruturais que o 
cluster exibe em função do seu tamanho pode levar à descoberta de
novos materiais com propriedades potencialmente úteis para 
determinadas aplicações tecnológicas. Por exemplo, é conhecido o
fato de que as propriedades óticas de clusters puros, tal como 
o perfil do espectro de absorção ótica, parecem depender do elemento 
químico, do tamanho do cluster e da sua estrutura~\cite{Barcaro:2015}. 
A compreensão detalhada desta dependência pode sugerir métodos de 
manipulação ao nível at\^{o}mico (\emph{fine tuning}) que constituem
alternativas viáveis aos métodos convencionais de dopagem
das estruturas de \emph{bulk}.
Obter informações estruturais dos clusters diretamente a partir dos 
experimentos é ainda um processo bastante difícil e por isso 
é necessário, por enquanto, combinar resultados experimentais
com modelagem teórica.

A proposição principal do trabalho desta tese é de que explorar a
superfície de energia potencial de clusters at\^{o}micos, na busca das 
suas estruturas estáveis e meta-estáveis, tende a ser mais produtivo
com a utilização de algoritmos genéticos. Com isso, queremos dizer
que esta categoria de algoritmos de busca, com base no conceito de população, 
tende a explorar regiões mais extensas da superfície de energia, 
aumentando assim a chance de encontrar estruturas meta-estáveis 
interessantes e, quiçá, a estrutura global de energia mínima. Isso, no 
nosso entender, é mais difícil de se conseguir com algoritmos do tipo 
Monte-Carlo, como o \emph{basin-hopping} ou o \emph{simulated annealing},
que são essencialmente técnicas de busca local probabilística (com regras 
derivadas por analogia com a termodinâmica), mais propensos a ficar aprisionados
em estruturas de mínimo local.

O caminho inicial que procuramos seguir foi a determinação das estruturas de 
clusters neutros de carbono com o auxílio de um potencial interat\^{o}mico 
semi-empírico, isto é, um potencial ajustado para um conjuntos de propriedades
 de moléculas orgânicas e de estruturas cristalinas de carbono (diamante e 
grafite). Esta abordagem, que nós designamos por \emph{pre-screening}, traz 
benefícios em termos computacionais, porque
permite explorar estruturas de clusters com tamanho razoável (várias dezenas de 
átomos), mas a exploração da superfície de energia é feita com um nível de 
aproximação ``grosseiro''. Uma forma de preencher esta lacuna consiste 
na reotimização das estruturas com um nível de teoria quântico, como,
por exemplo, o DFT\@. Nós realizamos este procedimento com duas implementações
do método: a implementação mais comumente utilizada em química teórica, isto é,
os orbitais de Kohn-Sham são descritos com o auxílio do conjunto de funções 
de base, e a implementação segundo a qual os orbitais são discretizados
numa malha de pontos, método esse que designamos pelo acr\^{o}nimo 
RS-DFT (\emph{real space DFT}). Verificamos que as propriedades estruturais
dos clusters obtidos com potencial empírico foram razoavelmente confirmadas
pelos estudos de reotimização com DFT\@. Em particular, nós verificamos que
as propriedades estruturais, eletr\^{o}nicas e vibracionais dos clusters de carbono
são muito semelhantes entre as duas implementações do DFT, o que dá credibilidade
ao uso de técnicas de RS-DFT\@. Por se tratar de um método pouco 
conhecido na química teórica, seria interessante fazer uma comparação 
mais exaustiva entre as duas metodologias.
Contudo, quando os nossos resultados são confrontados com a literatura, 
subsistem algumas discrepâncias nas propriedades estruturais.
Embora haja um acordo total quanto à existência global dos diferentes 
tipos estruturais, como sejam, as estruturas lineares; as estruturas 
policíclicas (aneladas e do tipo grafeno), e os fulerenos, 
a discrepância está nos detalhes, mais propriamente, quais os tamanhos
críticos (número de átomos) onde se dá a transformação de um tipo 
morfológico para outro, á medida que o tamanho do cluster aumenta.
Uma das fraquezas da nossa metodologia de \emph{pre-screening} é precisamente
o uso de potenciais interat\^{o}micos na exploração inicial da superfície de
energia. Por isso é importante desenvolver um algoritmo de busca onde as
optimização das estruturas seja feita ao nível quântico.
Isso leva-nos ao algoritmo QGA \emph{quantum genetic algorithm} que foi 
testado com clusters de metais alcalinos (sódio-potássio). Nesse trabalho,
mostramos que o nível de teoria MP2 é competitivo em termos de desempenho
computacional com o DFT na busca de estruturas de energia mínima.
Contudo o nosso estudo ficou ainda restrito a clusters de tamanho pequeno 
(\( \le 9 \)), por isso continua sendo imperativo melhorar a eficiência do 
algoritmo genético.

Um dos problemas dos algoritmos genéticos utilizados no trabalho desta tese 
é o fato destes utilizarem a estratégia do tipo geracional, onde toda uma 
geração de indivíduos é substituída na fase de reprodução e mutação por 
novos indivíduos.
Do ponto de vista computacional isto introduz um gargalo serial porque
o algoritmo tem que aguardar que todas as estruturas dos membros da
população sejam localmente relaxadas (um passo bastante caro) para depois 
prosseguir para a próxima geração. Uma forma de aumentar a eficiência 
computacional do algoritmo seria otimizar as estruturas de forma 
distribuída (paralela), mas o gargalo serial acima referido continua a 
ser impactante.
Existe, no entanto, um outro tipo de algoritmo genético mais apropriado 
para o paralelismo, designado por algoritmo genético de estado estacionário
(AGEE), onde há coabitação de indivíduos de gerações diversas, ou seja, é
eliminado o conceito de geração única em cada passo do 
algoritmo~\cite{Luke2013Metaheuristics}.
Uma vez que um indivíduo é gerado (possivelmente acompanhado de mutação) 
ocorre uma decisão sobre entre a sobrevivência do novo indivíduo e algum da
população atual. Várias estratégias poderão ser seguidas, desde o novo
individuo substituir o pior da população, ou então substituir um individuo
escolhido aleatoriamente da população; executar o torneio, onde um 
grupo de indivíduos é selecionado aleatoriamente e o pior indivíduo deste 
grupo é substituído. Outro exemplo seria substituir o indivíduo mais antigo 
da população, mas esta estratégia tem o inconveniente de ter a probabilidade
alta de substituir um dos indivíduos mais adaptados da população.
As estratégias acima podem usar o elitismo para preservar ao máximo
os melhores indivíduos já encontrados.
O algoritmo pode ser construído de modo a manter o tamanho da população 
(``pool'') constante e o seu critério de parada pode ser estabelecido 
quando a energia mínima do ``pool'' variar menos do que um valor 
pré-definido de diferença de energia dentro de um certo número sucessivo de
otimizações de estrutura.
Na área da físico-química a primeira sugestão de aplicação deste tipo de
algoritmo foi feita por~\citeonline{Bandow-2006-ID639}. Seguiram-se
outros trabalhos onde foram implementadas variantes deste conceito:
\citeonline{Dieterich-2010-ID627,Shayeghi-2015-ID1590}.
O interessante a notar aqui é que os operadores genéticos
podem ser aplicados aos indivíduos do ``pool'', juntamente com as otimizações
 das suas estruturas, de uma forma completamente distribuída, independente
 e assíncrona, como mostra a~\autoref{fig:pool_ga}. Com isto é possível 
obter em tese ganhos computacionais substanciais que cobrem o 
tempo gasto com cálculos quânticos caros de otimização.
%------------------------------------------------------------------------------
\begin{figure}[!hb]
\begin{center}
\includegraphics[width=0.9\textwidth]{conclusion_figs/pool_ga.pdf}
\end{center}
\caption{\label{fig:pool_ga} Diagrama esquemático do novo conceito de algoritmo
genético paralelo, designado no texto por algoritmo genético de estado
estacionário, AGEE\@. Um grupo de processos (\emph{workers}) aplica de forma
independente e assíncrona os operadores genéticos nos \( N \) indivíduos
do ``pool''. Um  processo adicional (\emph{master}) pode monitorar a convergência
da população e garantir a constância do seu tamanho.}
\legend{Fonte: adaptado de~\citeonline{Bandow-2006-ID639}}
\end{figure}
%------------------------------------------------------------------------------



